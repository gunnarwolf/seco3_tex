\chapter[La construcción colaborativa del conocimiento desde\\ la óptica de las comunidades de software libre]{La construcción colaborativa\\ del conocimiento desde la óptica\\ de las comunidades de software libre}
\chapauth{Héctor Colina}

\lefthead{Desde la óptica de las comunidades de SL}

\epigrafe{Porque habiendo proporcionado una~dádiva a~los~mortales estoy uncido al~yugo de~la~necesidad, desdichado.}{Esquilo\\Prometeo encadenado}

\introduccion

\noindent  El presente capítulo aborda la construcción colaborativa del cono\-ci\-miento según las comunidades de software libre, a la luz del conocimiento libre, de la apropiación tecnológica y los procesos de transferencia de tecnologías.

 Desde un punto de vista amplio de lo tecnológico, es posible
 encontrar elementos para generar una perspectiva dialéctica en las discusiones sobre puntos esbozados en los discursos científicos actuales como el de la neutralidad tecnológica, patentes industriales, propiedad intelectual y otros más.

 Esta perspectiva permite abordar, desde una aproximación diferente, el tema de la construcción colaborativa del conocimiento en comunidades a las que como premisa definen y cohesionan, por ejemplo, las comunidades de software libre.

 ``Lo que cedas se reproducirá'' pareciese una máxima tácita en el mundo del desarrollo comunitario libre que, a primera vista, choca con prácticas o sistemas ya establecidos, en los cuales el valor principal deviene de lo que ``vendas'' o aportes al mercado en que se colocan los productos de la inventiva del desarrollador.

 En la esfera de acción del desarrollo comunitario, nos proponemos analizar algunos factores comunes que nos posibilitan vislumbrar elementos y prácticas comunes a diversas comunidades de desarrollo. En esencia, tratamos de reflexionar sobre dos ideas:

\begin{enumerate}
  \item ¿Qué diferencia a las comunidades de desarrollo abierto y libre de otras comunidades de desarrollo?

  \item ¿Qué hace que los individuos participen en las comunidades de desarrollo abierto y libre?
\end{enumerate}


 Esas dos ideas, nos permitirán, entonces, adelantar reflexiones sobre algunos hechos que parecen obvios en un primer análisis. Aunada a esas dos interrogantes necesitaremos, también, examinar el concepto de ``tecnología'' para así ampliar las perspectivas del análisis que nos proponemos y entonces acercarnos a la forma en que se construye el conocimiento en las comunidades de software libre.

\section{Prolegómeno}

\noindent  El concepto de ``tecnología'' que prevalece en la actualidad ha sido amoldado desde una visión unilateral, rodeada por un aura de ``cientificismo'', de artefacto tangible que exhibe un grado de complejidad en función de la cual el aparato tendrá, en igual proporción, mayor o menor tecnología.

 No obstante, una aproximación diferente, en la cual el concepto de tecnología se amplíe a fenómenos como las interrelaciones entre el objeto tecnológico definido en términos clásicos y el individuo que lo ``posee'' y, a su vez, entre el individuo propietario del artefacto tecnológico, el entorno y otros individuos, permitirá contar con una percepción más amplia, más enriquecedora e incluyente.

 En ese orden de ideas, es factible observar que cuando un grupo de individuos con un fin común (comunidad) se relacionan en torno a un objeto tecnológico, pueden generar conocimiento desde diferentes puntos sobre el objeto que les convoca.

 Tal prolegómeno sirve de base a esta disertación sobre las vías de construcción del conocimiento en las comunidades de software libre, para lo cual nos serviremos de prácticas consagradas en dichas comunidades (por ejemplo la adopción de nuevos paquetes, flujos de trabajo y otros), revisión de bases teóricas y la experiencia propia.

\section[Tecnología y comunidades de software libre]{Tecnología y comunidades\\ de software libre}

\subsection{Repensar la tecnología}

\noindent  La posesión del ``conocimiento'' ha sido clave a lo largo de la historia, al dar ventajas de poder y sabiduría a quien lo ejerce. La mitología y la religión son prolijos en ejemplos, entre ellos: Prometeo, al robar el fuego a los dioses, fue condenado no tanto por su atrevimiento sino por dejar a los dioses sin el conocimiento; la serpiente que incita a Eva a que coma la fruta del árbol de la sabiduría, fruto que condena a los habitantes primigenios del paraíso terrenal a vagar y a sufrir.

 Desde esa perspectiva, la aplicación de ese conocimiento, la concreción del mismo en productos define a la ``tecnología''; la segunda raíz de esta palabra griega, \textit{logos},\footnote{Significa \Parencite{721}: la palabra en cuanto meditada, reflexionada o razonada; es decir, ``razonamiento'', ``argumentación'', ``habla'' o ``discurso''. También puede ser entendido como ``inteligencia'', ``pensamiento'', ``ciencia'', ``estudio'', ``sentido''. } permite observar la impronta del concepto en Occidente.

 Sin la apreciación de que la tecnología es más que ese conjunto de saberes aplicados, no podremos vislumbrar un panorama mayor como el que divisa Heidegger \Parencite{722}, cuando dice que ``[\ldots] la esencia de la técnica tampoco es en modo alguno nada técnico'' refiriéndose a que dicha esencia de la técnica \Parencite{723} no tiene nada que ver con la técnica en sí, sino con elementos relacionados más con lo humano.\footnote{Según Heidegger \Parencite{724}, ``[\ldots] es una consecuencia y destinación del ser y del olvido del ser''.}

 Desde la perspectiva cultural occidental, lo tecnológico es un \textit{cuasi} sinónimo de aparato complejo, tangible o intangible, sobre el cual hay todo un conglomerado de teorías,\footnote{Tecnologías web, tecnologías de programación, por ejemplo.} pero\ldots ¿serán esos los únicos elementos que puedan considerarse tecnologías?

 Óscar Varsavsky \Parencite{725} proponía diferenciar entre ``tecnologías físicas'' y ``tecnologías sociales''. Las primeras, básicamente ``comprenden los instrumentos o métodos para alcanzar ciertos objetivos concretos de producción'' \Parencite{726}, mientras que las segundas gravitan en el acto de la producción ``pero de producción en su sentido más amplio: no sólo de bienes sino de servicios de tipo cultural, político e institucional de infraestructura''.\footnote{Ídem.} Los elementos de análisis citados nos permitirán observar el hecho tecnológico desde otro punto de vista, uno que vaya más allá de lo ``neutral'' \Parencite{727}, para entonces concebir el hecho tecnológico en forma holística, ecuación en la cual el hombre afecta y es afectado.

\section{El conocimiento es un hecho social}

\noindent  Aprendemos en la medida en que nos relacionamos con los demás;\footnote{Al respecto, refiérase a Ausubel \parencite*{718} y Bruner \parencite*{719}, quienes han desarrollado sendas teorías.} es decir, el acto de ``aprender'' es un acto social, un constructo comunitario \Parencite{720}. 

En la medida en que aprendemos, generamos conocimiento que, tarde o temprano implementaremos de una forma u otra. Al respecto, Bruner \Parencite{717} señala que la ``estructura cognitiva previa del aprendiz (sus modelos mentales y esquemas) es un factor esencial en el aprendizaje''. Esta estructuración previa nos posibilita examinar aspectos del conocimiento y aprendizaje en las comunidades de software libre.

\section{Comunidades de software libre}

\noindent  ¿Qué es una comunidad? Debemos empezar por esta pregunta
antes de extender el concepto al de comunidades de software libre.

Para nosotros, una \textit{comunidad} es un grupo de individuos con un fin común, objetivo tangible que los agrupa, que les motiva a seguir, a continuar. Al momento de alcanzar el objetivo común, la comunidad podría desaparecer por falta de dicho elemento de cohesión.

 Aunado al fin común, un rasgo importante en las comunidades es el hecho comunicativo; por ejemplo, Luhmann \Parencite{715} reconoce sistemas autopoiéticos sociales\footnote{Retomando los conceptos de Maturana y Varela \Parencite{716}.} que permiten a las comunidades regularse y perdurar en el tiempo y uno de ellos es el acto comunicativo: difícilmente una comunidad puede mantenerse si en el acto de marchar hacia un fin común no hay un proceso comunicacional que permita una autopoiesis de ella misma. Ahora es menester, entonces, que nos preguntemos sobre las comunidades de software libre y su interacción con la tecnología y la forma en que producen conocimiento.

\section{Elementos para entender las comunidades de software libre}

\noindent  Para nuestro análisis, partiremos de un concepto basado en experiencia propia y en trabajo previo en las comunidades de software libre: grupo de individuos con ``prácticas virtuosas'' que unidos por un fin común trabajan bajo ``modelos no tradicionales de desarrollo'' y desde modelos socioproductivos enfocados en ``servicios'' \Parencite{791}.

 Para ahondar en ese concepto, conviene clarificar los preceptos bajo los cuales lo estamos construyendo. Una práctica virtuosa es la que no posee un reconocimiento académico formal, sino derivado de la maestría en la ejecución de las labores, y este virtuosismo debe ser reconocido y aceptado en el grupo en el cual se genera; así, un programador de software podrá ser virtuoso aun sin un grado académico y tenido como tal en la comunidad donde haga su práctica. Un modelo no tradicional de desarrollo se genera en comunidades en que el fin ulterior del proyecto no es el elemento económico; para ello, se basa en elementos como la generación de conocimiento colectivo, el aprendizaje social, las prácticas heterárquicas de toma de decisión y otras más.

 Para finalizar esta sección de términos, necesitamos ahondar en el concepto de heterarquía, ya que es clave para conocer en qué forma se dan los \textit{modelos de gobernanza}, los cuales exhiben una particularidad: una autoridad consensuada, aceptada y no impuesta, al contrario de otros modelos organizacionales.

 Estas ``figuras visibles'' son emergentes en función de dinámicas que ``emergen'' en un momento determinado ante una necesidad específica; así, surgen ``líderes'', ``caras visibles'', ``articuladores'' que impulsan una visión común, consensuada de la comunidad.
