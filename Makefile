BASENAME = seco3

all: pdf
	evince $(BASENAME).pdf

dvi: epss biblio
	latex $(BASENAME)

pdf: biblio *.tex
	pdflatex $(BASENAME)
	touch pdf

epss:
	for i in *png; do \
	  echo == $$i; \
	  convert $$i $$(echo $$i | sed s/png/eps/); \
	done
	touch epss

clean:
	rm -f *.blg *.aux *.log *.out *.toc *.bbl *.-blx* *.idx *.xml *.dvi html/

realclean: clean
	rm -f pdf *.pdf epss *.eps first_run

biblio: *.tex first_run
	for i in *blx.aux; do \
	  bibtex $$i; \
	done

first_run:
	pdflatex $(BASENAME)
	touch first_run

html: first_run biblio
	mkdir -p html
	latex2html --init_file latex2html-init -dir html \
		-split 3 -toc_depth 3 -show_section_numbers \
		-local_icons -iso_language ES -no_footnode \
		--numbered_footnotes -info '' -html_version "4.0,unicode" \
		-t 'Construcción Colaborativa del Conocimiento' $(BASENAME)

por_cap:
	for cap in presentacion cap1 cap2 cap3 cap4 cap5 cap6 cap7 cap8 apend1 apend2 apend3 apend4; do \
		item=seco3_$$cap ; pdflatex $$item && bibtex $$item && pdflatex $$item && pdflatex $$item; \
	done
