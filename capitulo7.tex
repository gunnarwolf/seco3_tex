\chapter[Analfabetización informática o ¿por qué los programas\\ privativos fomentan la analfabetización?]{Analfabetización informática\\ o ¿por qué los programas privativos fomentan la analfabetización?}
\chapauth{Beatriz Busaniche}

\lefthead{Analfabetización informática}

\introduccion

\noindent  En los inicios del siglo \textsc{xxi}, resulta común escuchar aseveraciones que indican que no utilizar o no tener acceso a las nuevas tecnologías de información y comunicación constituye una nueva forma de analfabetismo, el informático. Se afirma que quien no sabe manejar una computadora es un analfabeto digital. Vamos a realizar un análisis crítico de esta cuestión.

 Parece indispensable reducir la denominada ``brecha digital'', para lo cual se destinan recursos y se realizan reuniones de carácter internacional dirigidas a paliar la diferencia entre los ``conectados'' y los ``no conectados'', sin atender que la brecha digital es subsidiaria y consecuencia directa de las brechas sociales básicas, la pobreza, el hambre, la exclusión, el analfabetismo, la miseria. Llevar computadoras a las zonas menos desarrolladas no hace más que informatizar la pobreza, al tiempo que canaliza fondos de cooperación e inversión internacional o endeuda a los mismos países que se pretende favorecer para sostener el negocio de las empresas de telecomunicaciones y las corporaciones de tecnologías.

 Frente a estos hechos que dan lugar a numerosos programas orientados a brindar acceso a las tecnologías de la información y comunicación (\textsc{tic}), conectividad y educación informática en escuelas e instituciones intermedias, se torna necesario preguntar qué entendemos por alfabetización y cuál es la alfabetización informática que queremos. Poco se habla del software como lenguaje cultural de nuestra era y mucho menos sobre la necesidad de aprender y apropiarse de ese lenguaje como única forma de acción participativa en la era de la información. Si no lo asumimos y actuamos en consecuencia, en realidad habremos de generar un enorme y mayor ejército de analfabetos futuros.

 Partimos del supuesto de que hay por lo menos dos formas de asumir la relación con las tecnologías:

\begin{description}
\item[La de consumidores pasivos.] Reduce a la persona a un acceso a la tecnología en condiciones de ``receptor pasivo'' o con escasa participación siempre encuadrada en un formato dado e inmodificable (radio, televisión y computadoras usadas como máquinas de escribir).

\item[La de ciudadanos.] Personas capaces de crear y compartir cono\-ci\-miento en la red, que utilizan las \textsc{tic} para crear y construir culturas y nuevas relaciones sociales. Ciudadanos capaces de crear una red multinodal de difusión y que pueden construir sistemas de información porque comprenden, hablan y ponen en práctica la lengua informática. Estos ciudadanos usan la computadora como centro y gestión de comunicación.
\end{description}



 Hay poderosos intereses que pretenden marginar a enormes segmentos de la población del mundo a meros consumidores. Nuestro punto de vista se orienta a impulsar un mundo virtual distinto, basado en la libertad y el acceso de pleno derecho. Nada de esto es posible sin una ciudadanía alfabetizada, capaz de crear comunidades y redes como internet, compartir y liberar el conocimiento.

 Nuestra hipótesis es que educar con software privativo fomenta la analfabetización informática. Esta aparente contradicción con la mayoría de los programas de alfabetización que conocemos en los ámbitos local y global postula la idea de que mientras más software privativo usemos \Parencite{244}, formaremos más personas incapaces de superar los desafíos de un jardín de infantes. Veamos por qué.

\section{¿Qué entendemos por alfabetización?}

\noindent  Estamos en el denominado ``decenio de la alfabetización (2003-2013)'' proclamado por Naciones Unidas; tiempos de fundamental importancia en los que se plantea una preocupación generalizada. La Declaración Universal de los Derechos Humanos \Parencite{242}, en su artículo 26, reconoce y proclama el derecho a la educación que debe ser gratuita al menos en la instrucción elemental y fundamental.

 Pero esta declaración que las naciones del mundo han proclamado se halla lejos de lograr sus objetivos de ``educación para todos''. Sin ir más lejos, los datos de la \textsc{unesco} indican que en el mundo hay 860 millones de adultos analfabetos y más de 100 millones de niños sin acceso a la escuela. El objetivo de la educación para todos es un imperativo que si bien se acepta en papel en el ámbito global, difícilmente encuentra correlato en las políticas locales de los países en vías de desarrollo.

 Según la misma \textsc{unesco}, la alfabetización va mucho más allá de las capacidades de leer y escribir. La alfabetización implica saber comunicarse en sociedad, generar y mantener prácticas y relaciones sociales, comprender el lenguaje y la cultura.

 En líneas generales, una persona se considera ``alfabetizada'' no sólo cuando aprende a leer, sino también cuando adquiere la capacidad de comunicación escrita y puede utilizar todo el potencial de las herramientas comunicativas esenciales para su interacción en la sociedad.

 Sólo quienes no están realmente alfabetizados conocen la magnitud de la exclusión. Cuando la comunicación se basa solamente en la interpretación de imágenes, sin disponer de la capacidad de leer y escribir, queda de manifiesto que la igualdad de derechos postulada en la declaración universal de los derechos humanos está muy lejos de ser una realidad para todos.

 Analizar las bases conceptuales de la alfabetización nos permite afirmar que una persona que sólo se comunica por medio de simbología iconográfica no está realmente alfabetizada.

 El acceso activo a la cultura implica no sólo observar, sino participar. Hagamos un paralelismo con la música. Sentarse a oír música sólo fomenta un papel pasivo frente al arte, el del ``escucha''. Es como aprender a leer, pero no saber escribir; el arte de la música no tiene nada que ver con la pasividad. Para ser activo participante de la música hace falta entender su lógica incluso desde una perspectiva matemática. La recepción pasiva es una faceta primaria del acceso a los sonidos. Un involucramiento activo debería incluir no sólo el entendimiento de la lógica, sino la capacidad de manipular, modificar y hacer música.

 Algo similar ocurre con la informática. Aunque aplicado a este campo, el papel de pasividad es aún más problemático. La música, como la informática, se basa en lenguajes-código que es necesario dominar para comprenderla plenamente. Al igual que la lengua, la música y la informática se construyen en forma de textos que se interpretan de diferentes formas.

 En música, el texto se escribe sobre partituras, se constituye con indicaciones de armonía y ritmo que combinados y luego interpretados generan los sonidos. Los músicos necesitan comprender esos ``textos'' para interpretarlos y escribirlos. En cambio, en computación, los seres humanos no requieren leer el código informático para ejecutarlo, ya que son textos escritos para que los interpreten máquinas, no personas. Las máquinas los interpretan por nosotros.

 Y ahí es donde se puede llevar adelante la trampa. Allí es donde se confunde la ejecución con el texto. Y es allí donde se esconde la tramposa idea de que los ``usuarios básicos'' no necesitan acceso al código fuente de los programas y, por tanto, no necesitarían leer y escribir nada sobre esos textos. Esto, sin lugar a dudas, es una falacia que sirve para justificar la analfabetización informacional.

\section{Contenido y estructura}

\noindent  No estaría de más volver a citar la frase de Marshall Mc Luhan de que ``el medio es el mensaje'', porque sin lugar a dudas es tan precisa y potente que vale la pena recuperarla una vez más. La separación de ``contenido'' y ``estructura'' de la información no hace más que negar esa afirmación tan usada en multiplicidad de textos. Si el medio es el mensaje, el soporte y el contenido de la información no deberían separarse con tanta facilidad.

 La mayoría de los planes de una mal denominada ``alfabetización informática'' apuntan a enseñar el uso de herramientas predeterminadas y desarrolladas por alguna empresa, casi siempre multinacionales del software, para generar ``contenidos'' de diferentes temáticas. Se habla mucho sobre el uso pedagógico de los \textit{weblogs}, por poner un ejemplo, y se enseña a manejar procesadores de texto, planillas (u hojas) de cálculo, programas de dibujo, de manipulación de imágenes y demás programas para ``editar contenido''; pero nada se dice sobre los límites inherntes a estos programas.

 Hablar de ``contenido'' nos obliga a pensar en una forma de llenar algún ``formato'' preestablecido, con seguridad fijado por otro, independiente de quien está trabajando sobre el contenido. El ``contenidista'' no tiene ninguna posibilidad de interactuar con la herramienta más que con las funciones predeterminadas en el diseño de la misma; sólo llena un espacio prearmado, está cautivo de la herramienta y debe adaptarse a ella.

 Un programa de alfabetización que sólo apunte al contenido no hace más que generar ``rellenadores de espacios en blanco'' que otros han diseñado por ellos, personas limitadas a crear o leer lo que la estructura les permite y autoriza, y absolutamente nada~más.

 Trabajar con programas privativos nos reduce al papel de simples ``contenidistas'' según formas prearmadas por un tercero.

 Sin acceso al código fuente no hay educación informática posible, sino sólo formación de personas privadas de conocer en profundidad los programas informáticos, que serán usuarios y consumidores de desarrollos ajenos.

\section{El futuro en el jardín de infantes}

\noindent  Hay numerosos lenguajes en el mundo: lenguajes para escribir música, para escribir historias, para escribir programas informáticos. Sólo el conocimiento de estos lenguajes propicia un acceso activo y participativo a diferentes áreas y prácticas. Sin embargo, los programas de seudoalfabetización informática pretenden ``facilitar'' el acceso de tal manera que las personas no tengan que interactuar con el código ni molestarse en aprender un lenguaje que parece vedado a unos pocos, lo cual se equipara a enseñar juegos de encastre de jardín de infantes y jamás profundizar en el entendimiento de la geometría y la matemática.

 En nuestros sistemas educativos no permitiríamos de ninguna manera que un alumno de escuela secundaria aprendiera sólo a encastrar cubos en donde entran cubos, y triángulos donde entran triángulos; nadie podría considerar alfabetizada a una persona que no sea capaz de ir más allá. Pero toleramos y promovemos esto en un área crucial como la informática.

 La pedagogía basada en software privativo busca hacernos creer que el aprendizaje informático es seguir encastrando cubos. Por más habilidades que desarrollemos para rellenar espacios prearmados con contenidos propios, continuaremos atados a un formato ajeno, específicamente a un formato incuestionable e inmo\-di\-fi\-cable.

 Enseñar el ``uso'' de software privativo en educación básica es condenar a nuestros niños a seguir con juegos de encastre, a convertirse en meros ``contenidistas'' de un formato prearmado y a acotar sus capacidades creativas a las posibilidades limitadas de y por una herramienta que diseñó alguien ajeno, muchas veces ajeno al propio país y región. Allí es donde el software privativo fomenta el analfabetismo. Veamos:

\begin{description}
\item[Acota las posibilidades creativas.] Obliga a nuestros niños a usar cierto número de tipografías, dibujos, cálculos, reglas prefijadas desde afuera. Lleva a que los niños deban adaptarse a la herramienta, y no la herramienta a ellos. Y una vez que se acostumbraron a este uso, devolverles la capacidad de cuestionar la herramienta y modificarla implica una tarea mucho más compleja.

\item[Condena a la comunicación iconográfica.] Si partimos de la consideración que las personas que sólo son capaces de comunicarse mediante simbología iconográfica no están suficientemente alfabetizadas, ¿por qué promover este tipo de alfabetización limitada cuando se trata de programas informáticos? Las potencialidades de aprendizaje se desarrollan más en la niñez, y allí hay que comenzar a trabajar con los lenguajes que constituyen nuestro universo cultural. Sin duda, en la era de la información digitalizada, el software se vuelve una técnica cultural cada vez más imprescindible.

\item[Educa] a nuestros niños para que sean estrictamente usuarios de determinadas marcas en vez de ser activos participantes en la construcción dinámica y comunitaria de software.

\item[Obliga] a educar a nuestros niños en el egoísmo de que ``está prohibido compartir'', a riesgo de convertirse en delincuentes por intercambiar juegos y programas con sus compañeros, familia y comunidad y a ``no investigar'' las herramientas con las que aprenden porque no sólo se prohíbe el estudio de las mismas, sino que se castiga la ingeniería inversa que se pudiera aplicar sobre ellas.
\end{description}



 Educar con software privativo nos obliga a borrar con el codo todo lo que podemos escribir con la mano en términos de solidaridad, ética y derechos, y a reprimir la curiosidad de los alumnos por entender cómo funcionan los programas.

 En un mundo donde el código es ley \Parencite{243}, se torna cada vez más indispensable saber quién tiene la capacidad de escribir y comprender esas leyes y generar esas potencialidades en el ámbito local. Si educamos con software privativo difícilmente lograremos en el futuro una generación de ciudadanos redactores y analistas del código clave y estratégico en nuestras vidas y decisiones, que hagan aportes sustanciales al desarrollo de nuestra región. Y aún peor, las leyes que regulen nuestras actividades serán escritas por una élite selecta y reducida que tal vez carezca de relación con nuestra localidad.

 Si nuestra educación se basa en la iconografía, será siempre la computadora (más bien, la empresa que haya programado el software) lo que domine las decisiones de una persona, incluyendo lo que lea y escriba, y no la persona la que maneje su propia computadora; sobre todo cuando vemos que cada día avanzan más los dispositivos de control técnico de gestión de contenidos y materiales con \textit{copyright}. Cada vez más la regulación de acceso a la cultura se hace mediante software y hardware.

 Los países en desarrollo no pueden darse el lujo de invertir en \textit{desalfabetizar} a sus generaciones futuras.

\section{Perspectivas y propuestas}

\noindent  Por el momento, la perspectiva no puede ser más que pesimista. Lamentablemente la informática se ha convertido en una ciencia de arduo acceso para la mayoría de la gente. El común de las personas no desea siquiera entender qué hay dentro de esa caja misteriosa que forma el cuerpo de la computadora, y mucho menos enfrentarse a una línea de comandos.

 El daño producido por el intenso uso de software privativo a lo largo de los últimos 10 o 15 años será difícil de superar, sencillamente porque la gran mayoría de la gente ve la computadora sólo como una herramienta para realizar acciones específicas: navegar y buscar información, enviar mensajes de correo electrónico o redactar textos. La posibilidad de usar la computadora como un instrumento para desarrollar sistemas queda reducida exclusivamente a profesionales, cuando hay innumerables casos en que los ciudadanos podrían aprovechar la potencia expresiva de las lenguas informáticas para resolver problemas y necesidades, construir arte y tecnologías.

 El uso de software privativo no sólo produce una forma de acostumbramiento que podría asemejarse a una droga, sino que fomenta el facilismo y la comodidad. Es normal que la gente prefiera que la máquina funcione sola, antes que sentarse algunas horas a pensar cómo funciona y por qué. La informática se ha convertido en una especie de torre de marfil a la que sólo acceden personas elegidas. Las empresas de software privativo han facilitado la distribución masiva de software sencillo y básico en su uso, que no requiere demasiado análisis ni comprensión por parte del usuario. Muchos ven en ello una forma de ``democratización'' de las nuevas tecnologías, cuando en realidad se trata de una forma de dominación bastante confortable que nos hace sentir inmersos en una sociedad nueva, la de las redes de comunicaciones, pero en la que elegimos día a día cuál será la forma más agradable de nuestra sumisión.

 Apelar al uso de \textsc{gnu}/Linux por una cuestión de costos ya parece ser una obviedad. En países en vías de desarrollo, con alto porcentaje de pobladores por debajo de la línea de pobreza, gastar dinero en cajas y permisos de uso de software para las escuelas y la administración pública significa literalmente una forma moderna de malversación de fondos públicos; porque ya hay una enorme cantidad de distribuciones libres que se pueden incorporar al sistema educativo, incluso de desarrollo local.

 Pero estos avances tienen su contraparte en los planes de educación que se plantean desde las empresas de software privativo, dispuestas a realizar donativos de licencias para mantener el monopolio que detentan y a promover una vez más el facilismo y la resistencia al cambio. Entonces, el argumento de los costos ya no nos resulta suficiente; hay que apelar a la libertad, valor que lamentablemente muchos sacrifican para reducir esfuerzos y costos. No hay comodidad que justifique sacrificar el derecho a ``tomar parte libremente en la vida cultural de la comunidad, a gozar de las artes y a participar en el progreso científico y en los beneficios que de él resulten'' \Parencite{242}.

 Frente a esta perspectiva pesimista, algunos datos abren una puerta de salida. En el mundo hay una enorme comunidad de desarrolladores de software libre que han logrado la mayor construcción colectiva de las últimas décadas. Sin embargo, esa misma comunidad enfrenta enormes dificultades para que su obra y su mensaje se dispersen y sienten las bases de una verdadera alfabetización informática. La comodidad promovida por el software privativo puede más que muchos esfuerzos. El \textit{marketing} de esas mismas empresas es muy fuerte; y, muchas veces, las personas que deben tomar decisiones políticas no llegan a ver la magnitud de esta realidad.

 Es importante divulgar las actividades, desmitificar y abrir los caminos a la enorme cantidad de personas apasionadas por el arte de las lenguas informáticas, \textit{hackers} que construyen un movimiento de resistencia propio de la nueva era, con sus estrategias y armas, y que poco a poco socava el poder de las corporaciones del software que imponen sus moldes sobre la educación informática y criminalizan a quienes luchan por liberar, colectivizar y compartir el conocimiento.

 Si nos remontamos a los comienzos de la informática, encontramos programas y técnicas que permitían a cualquier persona resolver problemas simples por medio de lenguajes como \textit{basic} y otros que fueron populares en importantes sectores de usuarios. Pero con el correr de los años, las multinacionales hicieron cada vez más complejos y profesionales esos programas, elevaron los precios y los sacaron del ámbito del usuario común.

 Más datos nos ayudan a ser optimistas: el software libre ya concentra 70\% de los servidores web del planeta y permite sustentar una enorme diversidad de sitios, informaciones y conocimiento en la red, construidos con lenguaje \textit{html} que las personas se apropian con facilidad y utilizan para publicar y editar libremente.

 Pero los servidores en general están administrados por profesionales de la informática. Ahora nos toca lidiar con el desafío más complejo: llegar a los escritorios y al sistema educativo, para incidir realmente en los proyectos de alfabetización informática en las aulas y reivindicar el derecho a leer y escribir el código cultural de nuestra era.

 Un cambio posible podría llegar de la mano de varias medidas drásticas:

\begin{enumerate}
\item  Eliminar el uso de software privativo del sistema educativo (como mínimo del sistema educativo público).

\item  Fomentar una real alfabetización informática para docentes.

\item  Promover el análisis, estudio, mejora y modificación de los programas por parte de los alumnos y docentes.

\item  Promover, sostener y defender la libertad del conocimiento en todas las áreas educativas.
\end{enumerate}

 De lo contrario, jugaremos a los cubos de colores, por unos cuantos años más.
