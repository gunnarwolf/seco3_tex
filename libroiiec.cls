%% Archivo: FCmathbook.cls
%%

\NeedsTeXFormat{LaTeX2e}
\def\fileversion{v.1.0.4}\def\filedate{2007/01/25}
\ProvidesClass{FCmathbook}[\filedate\space\fileversion\space%
 Dise�o para los libros de matem�ticas de la Facultad de Ciencias UNAM]%%
 
\LoadClass[11pt]{book}
\RequirePackage{amsmath,amssymb,amsfonts,amsthm}
\RequirePackage{url}
\RequirePackage{fancyhdr}
\RequirePackage{titlesec}
\RequirePackage[ignoremp,papersize={14cm,21cm},textwidth=10.2cm,textheight=19.5cm,margin=1.5cm, %14.5cm - 23cm,margin=2cm, 
       includeall,nomarginpar,centering]{geometry} %textwidth=12.5cm,textheight=18cm (originalmente)
\RequirePackage{graphicx}
\RequirePackage[norule,splitrule]{footmisc}
\RequirePackage[cam,letter,dvips,center]{crop}%[cam,letter,dvips]
%%*** Redise�o del encabezado
\pagestyle{fancy}
\fancyhead{}
\fancyfoot{}
%% \fancyhead[LO]{\hfill{\scshape \nouppercase{\leftmark}}\hfill{\large\thepage}}
%% \fancyhead[RE]{{\large\thepage}\hfill{\scshape \nouppercase{\leftmark}}\hfill}
%\fancyhead[RE]{{\large\thepage}\hfill{\scshape \nouppercase{\rightmark}}\hfill} %%%%QUITAR
\renewcommand{\headrulewidth}{0pt}
%% \renewcommand{\splitfootnoterule}{\kern-3\p@ \rule[3pt]{2.5cm}{1pt}}
\renewcommand{\splitfootnoterule}{\kern-3\p@ \rule[3pt]{2.5cm}{1pt}}

\fancyhf{}
\headheight 38pt
%% \fancyfoot[CE,CO]{\thepage}
%% \fancyhead[LO]{\MakeUppercase{\leftmark}}
\fancyhead[RO]{\small\textsc{\leftmark}\qquad\thepage}
\fancyhead[LE]{\small\thepage\qquad\textsc{\rightmark}}
%% \renewcommand{\headrulewidth}{0.4pt}

\newcommand{\partordinal}{\textit{Parte indefinida}}
\newcommand{\partjoin}{: }

\titleformat{\part}[display]
  {\thispagestyle{empty}\Huge\filcenter}
  {\vfill\huge\partordinal}
  {1em}
  {}[\vfill\eject]

%% Las partes son ``falsas'', no les generamos ni n�mero de parte ni
  %% n�mero de p�gina en el TOC
\newcommand{\nocontentsline}[3]{}
\let\origpart\part
\renewcommand\part[1]{%
  { \let\addcontentsline\nocontentsline
    \origpart{#1}
  }%
  \addtocontents{toc}{
    \vskip 1em
    \noindent {\textsc{\partordinal\partjoin#1}}

  }%
}

%%*** Redise�o del formato del t�tulo del cap�tulo
\titleformat{\chapter}[display]
  {\normalfont\Large\scshape\flushright\nohyphens}
  {\scshape\chaptertitlename\ \thechapter}
  {0pt}{\scshape\huge\bfseries}{}
\titlespacing*{\chapter} {0pt}{2ex}{2ex}

% Redefinimos secciones, subsecciones omitiendo las negritas,
% siguiendo las recomendaciones del Depto. de Ediciones
%
% Formato de referencia tomado de
% http://mirror.ctan.org/macros/latex/contrib/titlesec/titlesec.pdf
\titleformat{\section}
            {\normalfont\Large\scshape\flushleft\nohyphens}{\thesection}{1em}{}
\titleformat{\subsection}
            {\normalfont\large\scshape\flushleft\nohyphens}{\thesubsection}{1em}{}
\titleformat{\subsubsection}
            {\normalfont\normalsize\scshape\flushleft\nohyphens}{\thesubsubsection}{1em}{}

\titlespacing*{\section} {0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing*{\subsection}   {0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}
\titlespacing*{\subsubsection}{0pt}{2ex plus 1ex minus .2ex}{1ex plus .2ex}

% Aligeramos las negritas
\renewcommand{\bfdefault}{sb}

% La descripci�n usa versalitas, no negritas
\renewcommand\descriptionlabel[1]{\hspace{\labelsep}\textsc{#1}}

% Eliminamos el espacio del m�rgen derecho en los ambientes quotation
\renewenvironment{quotation}{%
  \par\medskip%
  \leftskip=1.5em%
  \ignorespaces}{%
  \par\medskip}

% URLs en sans-serif (no monospace), corrigiendo detalles est�ticos/funcionales
%
% https://www.joachim-breitner.de/blog/archives/519-Nicer-URL-formatting-in-LaTeX.html
% Inspired by http://anti.teamidiot.de/nei/2009/09/latex_url_slash_spacingkerning/
% but slightly less kern and shorter underscore
\makeatletter
\urlstyle{sf}
\let\UrlSpecialsOld\UrlSpecials
\def\UrlSpecials{\UrlSpecialsOld\do\/{\Url@slash}\do\_{\Url@underscore}}%
\def\Url@slash{\@ifnextchar/{\kern-.11em\mathchar47\kern-.2em}%
    {\kern-.0em\mathchar47\kern-.08em\penalty\UrlBigBreakPenalty}}
\def\Url@underscore{\nfss@text{\leavevmode \kern.06em\vbox{\hrule\@width.3em}}}
\makeatother

\endinput
