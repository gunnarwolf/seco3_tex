\chapter*{Presentación}
\addcontentsline{toc}{chapter}{Presentación}
\lefthead{Presentación}
\righthead{Construcción colaborativa del conocimiento}

\noindent A lo largo de la historia de la humanidad, los procesos creativos en sus diferentes manifestaciones (científicos, tecnológicos o artísticos) se vieron limitados por la velocidad de la propagación de ideas y por la falta de contacto entre pares separados geográficamente. Con el transcurso de los siglos se han reducido paulatinamente esas barreras, y en la actualidad pasamos por un momento de inflexión, de modo que la manera de colaborar con nuestros pares está por dar un vuelco radical.

 El contacto por internet está cambiando la forma de producir conocimiento. En especial durante el siglo \textsc{xx}, con el constante desarrollo de medios de comunicación y transporte, la producción y transmisión de conocimiento se fueron globalizando; pero, en buena medida, se mantuvieron regidas por corporaciones de medios, ya sean comerciales, artísticos o académicos. El cambio proviene de la menor dependencia de intermediarios, de transitar del consumo a la producción en múltiples ramas del conocimiento y creatividad humanas.

 Amplios grupos han planteado nuevas dinámicas, y se han ido acercando y gravitando de manera natural unos hacia otros para plantear cómo será en el futuro el desarrollo del conocimiento y, en general, de los procesos creativos.

 Este trabajo deriva de la intención de formalizar, desde un punto de vista multidisciplinario, cómo va operando esta transformación.


\section*{El proyecto}

\noindent El presente proyecto nace de la experiencia de cuatro años de trabajo en el \href{http://www.edusol.info/}{Encuentro en Línea de Educación y Software Libre (Edusol).} En este encuentro de participación multidisciplinaria abordamos cómo opera la generación del conocimiento de forma colaborativa, geográficamente distribuida y bajo esquemas de licenciamiento permisivos. Más aún, ha sido muy importante descubrir cómo se plantea la \textit{transmisión} de dicho conocimiento a sus destinatarios (explícitos e implícitos).

 Con la seguridad de que el análisis ofrece oportunidades de reflexión que con mucho rebasan las dos semanas al año del Edusol, nos trazamos un proyecto más ambicioso: un seminario permanente para trabajar y debatir todo el año, de manera pública y mediada por la tecnología, lo mismo que la edición de un libro, éste, que recoge la participación de los autores invitados y de todos los interesados en colaborar con el proceso.

 Sobra decir que el \textit{experimento} de esta forma de colaboración for\-mó par\-te importante del Seminario y que, conforme avanzamos en el proyecto, las condiciones específicas de la participación y desarrollo se modificaron.



\section*{Forma de trabajo}

\noindent Este libro presenta ocho temas y cuatro apéndices, escritos por 10 autores procedentes de todos los rincones del mundo hispanohablante. El proyecto se elaboró con base en el planteamiento de un esquema de colaboración acorde con el tema.

\subsection*{Esquema de colaboración}

\noindent A pesar de la distancia y la diferencia disciplinaria de los autores, con el Seminario quisimos probar una forma de trabajo que pusiera en práctica el tema que desarrollamos como eje rector; en vez de limitar el trabajo a un tema por autor, pedimos a cada uno cuatro \textit{entregas} que reflejaran la evolución de su quehacer y permitieran la retroalimentación sobre un trabajo en proceso.

 Por otra parte, invitamos a las comunidades afines en las que participan los autores, ya que cada una de las entregas fueron presentadas al público interesado y se abrieron a comentarios. De igual modo, entre febrero y noviembre de 2009 sostuvimos \href{http://seminario.edusol.info/seco3/videoconferencias}{nueve videoconferencias, disponibles en el sitio Web del Seminario,} para exponer los temas.

 Los coordinadores reconocemos que la forma de colaboración propuesta no obtuvo la abrumadora respuesta que hubiéramos deseado, tal vez por la dificultad de mantener el interés de los participantes en seguir la evolución de un trabajo con un prolongado periodo de preparación; pero la participación en las sesiones de videoconferencias y mediante reseñas y comentarios en el sitio del Seminario fue siempre nutrida e interesante. Los videos y comentarios no forman parte del libro impreso por su naturaleza (en el caso de los videos) o por su estructura (en el caso de los comentarios), por lo que quedará disponible en la dirección Web \url{http://seminario.edusol.info/seco3} una vez publicada la presente obra.

 Aun cuando los temas se relacionan de muchas más maneras que las propuestas, para fines de estructuración aquí los temas del Seminario se organizan en tres partes principales, más los apén\-dices:

\begin{description}
\item[Software y medios libres.]
  Incluye capítulos introductorios al fenómeno social del software libre y de la cultura libre, ya que describen su génesis e inserción en los procesos históricos que han llevado al nacimiento de estos movimientos, lo mismo con una ideología explícita (como en el caso del movimiento del software libre) mediante el uso y la elaboración (como en las artes plásticas).
\eject

\item[Comunidades.]
  Si partimos de que la construcción del conocimiento es un fenómeno social, nuestro análisis abarca como punto fundamental las razones que llevan a los individuos a integrar comunidades y la manera en que éstas operan.

\item[Sociedad.]
  Profundiza en la forma en que los movimientos descritos se insertan y cómo inciden (o buscan incidir) en la vida de toda la sociedad.

\item[Apéndices.]
  Se exponen temas adicionales, relacionados con aspectos de la temática presentados en los demás capítulos, pero cuyo desarrollo no formó parte de la dinámica del Seminario. En aras de excluir material tan relevante, los hemos conservado y presentado como apéndices.
\end{description}




\subsection*{Manejo de referencias}

\noindent  Wikipedia constituye un experimento que se ha vuelto de uso prácticamente universal; nos ocuparemos de definirla y presentar brevemente su historia y características en la sección \ref{sl_cds:soft_al_conoc}.

 Dada la naturaleza de los temas y en pos de ser congruentes, en muchos de los procesos y fenómenos sociales descritos en la obra decidimos usar referencias a Wikipedia, lo mismo que referencias bibliográficas. Ahora bien, ante cuestionamientos de diversos colegas, e incluso ante la política oficial de Wikipedia de \href{http://es.wikipedia.org/wiki/Wikipedia_no_es_una_fuente_primaria}{no ser adoptada como fuente primaria,} vale la pena explicitar que nuestras referencias a Wikipedia cumplen con las siguientes condiciones:

\begin{itemize}
  \parindent 2em
\item Utilizamos las referencias a Wikipedia exclusivamente para brindar mayor información respecto a conceptos generales y ampliamente difundidos, y nunca  como fuente primaria. No la citamos para sustentar la veracidad de la información, sino para apuntar a definiciones más completas, en concordancia con lo que se indica en la página recién referida:

  \begin{quotation}
    Wikipedia es una enciclopedia. Como tal, su función no es actuar como un foro para el desarrollo del saber, sino recopilar y transmitir la suma del conocimiento acumulado y verificado en las distintas ramas de la actividad humana. Para garantizar su validez, la información que Wikipedia ofrece debe proceder de fuentes profesionales de reconocida seriedad. Los datos, conceptos, teorías o métodos que no cuenten con el respaldo de una publicación respetable, así como las interpretaciones o síntesis novedosas de los mismos, pueden ser sumamente valiosos como investigación original, pero Wikipedia no es el lugar para publicarlos o desarrollarlos, fundamentalmente porque no está diseñada para realizar la clase de revisiones y evaluaciones por las que el conocimiento científico debe pasar antes de ser publicado.
  \end{quotation}

\item Dado que la autoría de todo artículo publicado por Wikipedia es colectiva, todas las referencias a ésta se registran a su nombre, como forma única de referirse al colectivo de autores. Por lo expuesto en el apartado inmediato anterior, todo artículo que en ésta aparezca debe incluir referencias a fuentes primarias, aunque cada cita individual se hace a un artículo en particular, no a un texto escrito por un autor identificable.

\item Wikipedia cambia constantemente, a un ritmo que incluso nos ha sorprendido en el curso de la elaboración del presente trabajo. Una de sus característica relativamente poco conocida es que, además de que cualquiera puede editarla, guarda la historia completa de sus ediciones. Por ello, además de presentarse aquí la fecha de acceso, todas las referencias que le hacemos son a la revisión vigente en el momento de consulta; significa que todas las referencias bibliográficas que hacemos a ella conducen al estado de la definición referida en el momento en que el autor la consultó.

  En líneas generales, dado que Wikipedia se \textit{pule} paulatinamente conforme a sus ediciones, recomendamos al lector remitirse a la versión más reciente. Por rigor académico, no podemos referirnos a revisiones aún no efectuadas (revisiones en el futuro).
\end{itemize}



 Wikipedia es sólo un ejemplo. Entre las referencias encontrarán publicaciones no formales, como los artículos en \textit{blogs} personales. En esta obra sostenemos que nos encontramos ante un nuevo cambio de paradigmas que llevará a una mayor flexibilidad en la definición de lo que es o no fiable o primario. Confiamos en que, conforme se haga más común la publicación directa de trabajos incrementales, la \textit{referenciabilidad} de los recursos en línea, cambiantes y colectivos, será más aceptada y notas como la presente terminarán por resultar redundantes.

 En el desarrollo y las referencias de los diversos capítulos de esta obra, incluimos ligas a diversas fuentes en línea (Wikipedia, entre ellas). Las notas al pie ofrecen referencias de tipo mucho más casual y muchas veces no cuentan con la historia de ediciones como la ofrece Wikipedia, por lo que decidimos mantener el formato corto y descriptivo, para no distraer al lector con información relativa a la versión específica.




\section*{Implementación técnica}

\noindent  Consideramos de interés dar a conocer los aspectos generales que involucran a la implementación tecnológica de las diferentes facetas del Seminario:

\begin{description}
  \parindent 2em
\item[Plataforma Web.]
  Para la gestión de la interacción Web, empleamos el sistema de administración de contenidos (\textsc{cms}, por sus siglas en inglés) \href{http://www.drupal.org/}{Drupal.} Este sistema altamente modular tiende a convertirse más en un marco de desarrollo para aplicaciones Web que en un sistema de administración de contenidos. Los principales módulos utilizados fuera de la funcionalidad \textit{núcleo} fueron:

  \begin{description}
  \item[\textit{Bibliography.}]
    Maneja las bases de referencias bibliográficas y permite la exportación a BibTeX.

  \item[\textit{Content Construction Kit.}]
    Para la definición de tipos de contenido personalizados.

  \item[\textit{Footnotes.}]
    Facilita el manejo de notas al pie desde un documento html.

  \item[\textit{Notify.}]
    Envío au\-to\-má\-ti\-co de notificaciones al haber actualizaciones al contenido.

  \item[\textit{Simple news.}]
    Ges\-tio\-na el envío de boletines (por ejemplo, con noticias relativas al proyecto).

  \item[\textit{Organic groups.}]
    Permite crear grupos de usuarios con diferentes niveles de acceso.

  \item[\textit{User points.}]
    Asigna puntos (definidos por un usuario con permisos suficientes) a partir de acciones concretas como enviar un nodo o realizar comentarios.

  \item[\textit{Fivestar.}]
    Permite puntear la calidad de los contenidos en el sitio, empleando de una convención predefinida como puntos o estrellas; en nuestro caso se punteaban artículos, reseñas y comentarios.
\end{description}

\item[Entorno de videoconferencias.]
  Para contar con la participación más amplia posible, se optó por la superposición de dos bases tecnológicas muy diferentes para la video\-confe\-rencia:

  \begin{description}
  \parindent 2em
  \item[Red de videoconferencias.]
    La coordinación de los aspectos logísticos de una videoconferencia distribuida por varias sedes formales o institucionales en diversos países del mundo no es una tarea menor, sobre todo por la infraestructura y mecánica de trabajo de las salas de videoconferencia disponibles en las diversas uni\-versi\-dades.\footnote{El equipo que se usa en prácticamente la totalidad de estas salas se basa en implementaciones del protocolo \emph{H.323}, que se publicitan como productos ``caja mágica''. Hay también programas cliente libres, que permiten participar sin infraestructura formal; sin embargo, implican un protocolo en extremo sensible a ciertas condiciones de la red, con lo cual se convierten en una fuente inagotable de problemas t\'ecnicos.}

  \item[Flujo de video unidireccional]
    Dada la na\-tu\-ra\-le\-za distribuida de nuestro Seminario, fue fundamental que la mayor parte de los asistentes pudieran seguir nuestras actividades por medio de un \textit{stream}.\footnote{ Se denomina \emph{stream}  al flujo continuo de datos que permite a un usuario conectarse en cualquier momento a una transmisión multimedia en proceso, sin requerir verla desde su inicio.} A fin de transmitir las actividades hacia éste, y grabarlas en un formato adecuado para la gente que no pudo participar presencialmente en nuestro sitio, conectamos la salida de \texttt{dvgrab} con el recodificador \texttt{ffmpeg2theora}, hacia \texttt{oggfwd}, y un servidor \texttt{icecast}: \texttt{dvdgrab} toma la entrada de una fuente de video con el estándar \textit{Fi\-re\-wi\-re}; el programa \texttt{ffmpeg2theora} se encarga de recodificar flujos multimedia a un formato de tipo \textit{ogg};\footnote{ \emph{Ogg} es una colección de \emph{codecs} o formatos de audio y video, diseñada por activistas ideológicos del software libre con el objetivo específico de ofrecer un estándar libre, no sólo en lo relativo al código, sino a patentes.} \texttt{oggfwd} envía contenido \textit{ogg} sobre el protocolo \textsc{http}, y el servidor \texttt{icecast} se encarga de la distribución del flujo a todos los participantes interesados.

    Para permitir la retroalimentación, en un flujo de naturaleza unidireccional, contamos con un canal \textsc{irc} que permite a los participantes enviarnos sus comentarios e incluso platicar de modo informal entre ellos en el transcurso de la sesión. Las conversaciones derivadas del \textsc{irc} resultaron de gran interés para nosotros, y forman parte integral de la memoria electrónica del Seminario.

     En este apartado no podemos dejar de agradecer al Instituto de Investigaciones Económicas de la Universidad Nacional Autónoma de México (\textsc{iie}c-\textsc{unam}), por facilitarnos sus instalaciones para todas las sesiones solicitadas, así como a Carlos Cruz Barrera, coordinador y responsable de videoconferencias, por aguantar con gusto nuestras excéntricas solicitudes.
\end{description}

\item[Versión impresa.]
  El objetivo directo del Seminario planteaba ir más allá de crear un espacio de reflexión y trabajo: cristalizar nuestros esfuerzos en un documento impreso, que formalice los temas, facilite la consulta y pueda convertirse en una fuente referida. En consecuencia, la plataforma electrónica que seleccionamos nos permitió implementar el proceso de creación colaborativa. Imprimir directamente desde las páginas Web generadas resultaría práctico pero de una calidad insuficiente para cumplir con nuestras metas.

  Con la intención de editar un texto agradable de leer, elegimos convertirlo al sistema de tipografía \href{http://www.latex-project.org/}{\LaTeX,} descrito por muchos como el estándar \textit{de facto} para publicar documentación técnica y científica; \LaTeX ~es un conjunto de \textit{macros} creado por Leslie Lamport en 1985, con base en el lenguaje tipográfico \TeX, desarrollado por Donald Knuth a partir de 1978.

  Para convertir de un formato al otro, nos apoyamos en el programa \texttt{gnuhtml2latex}, y para los detalles finos de la lógica hicimos un \textit{script} en el lenguaje \textit{Perl}. Claro está, de este punto continuó el proceso editorial tradicional. Reconocemos y agradecemos la ayuda que brindó Héctor Miguel Cejudo Camacho, de la Facultad de Ciencias de la \textsc{unam}, para el formateo inicial del documento \LaTeX.

\end{description}



\section*{Acerca de los autores}

\authordata{Beatriz Busaniche}{Licenciada en Comunicación Social por
  la Universidad Nacional de Rosario. Trabaja para la Fundación Vía
  Libre desde 2003 y es integrante y fundadora de \textit{Wikimedia}
  \textit{Argentina}. Se desempeña como docente en la Universidad de
  Buenos Aires, en la carrera de Ciencias de la Comunicación de la
  Facultad de Ciencias Sociales.

  Ha colaborado en distintas publicaciones, entre ellas: \textit{Voto
    electrónico. Los riesgos de una ilusión}, \textit{Mabi: monopolios
    artificiales sobre bienes intangibles} y \textit{¿Un mundo
    patentado? La privatización de la vida y el conocimiento}. También
  ha participado en el seguimiento de varios procesos políticos
  relacionados con el software libre y la tecnología en
  general. Es activa conferencista en diversos eventos nacionales e
  internacionales.}{}{Argentina}

\authordata{Héctor Colina}{Licenciado en Teoría de la Historia
  Universal por la Universidad de Los Andes. Vive en Mérida,
  Venezuela, considerada una de las cunas del software libre en su
  país. Es miembro fundador de varios grupos, entre ellos Velug,
  Gulmer y Solve.

  Trabaja en el Centro Nacional de Investigaciones en Tecnologías
  Libres (Cenditel), en el área de apropiación tecnológica. Además,
  colabora en el proyecto Debian desde 2004 y desarrolla para el
  proyecto Canaima \textsc{gnu}/Linux, una iniciativa del gobierno
  venezolano para promover fortalezas sociotecnoeducativas. Se
  confiesa abierto amante del cine, la música, la lectura y del
  software libre.}{}{Venezuela}

\authordata{Carolina Flores}{Licenciada en psicología. Su trabajo se
  ha centrado en la comunicación social, las tecnologías de
  información y comunicación, las metodologías e implementación de
  procesos para los usos estratégicos de internet, la seguridad de la
  información y el cambio hacia el software libre.

  Es activista de la comunidad Software Libre Centroamérica. También
  forma parte del grupo Organizadas por Tecnologías y Recursos
  Abiertos Sostenibles (\textsc{otras}), un espacio de reflexión y
  coordinación de acciones para promover la participación de las
  mujeres en las comunidades de software libre y el acercamiento de
  más de ellas hacia la tecnología en general.

  Actualmente, coordina el proyecto Fortalecimiento de las Capacidades
  \textsc{tic} en Pymes y Gobiernos Locales Mediante el Uso de
  Software Libre, ejecutado por la Universidad Nacional de Costa Rica
  con el financiamiento del Programa de Naciones Unidas para el
  Desarrollo (\textsc{pnud}).}{}{Costa Rica}

\authordata{Antonio Galindo}{Programador y administrador de sistemas
  \textsc{gnu}/Linux desde hace 12 años, y miembro de la Fundación Linux. Ha
  participado desde 2004 como ponente en varios congresos nacionales e
  internacionales, relacionados con el software libre. Ha
  intervenido como organizador del Festival Latinoamericano de
  Instalación de Software Libre (\textsc{flisol}).

  Ha formado parte de diversos proyectos orientados a brindar acceso y
  fomentar el uso y la apropiación de la tecnología a la población de
  zonas desfavorecidas.  }{}{México}

\authordata{Alejandro Miranda}{Psicólogo educativo. Actualmente cursa
  el doctorado en Psicología en la Universidad Nacional Autónoma de
  México. Es activista de la educación con software libre, académico
  de la \textsc{unam}, y fundador y coordinador del Encuentro en Línea
  de Educación, Cultura y Software Libres.

  Ha participado como ponente en múltiples congresos nacionales e
  internacionales con temas relativos a las tecnologías de la
  información en la educación y sistemas de evaluación por
  computadora. De 2004 a la fecha se dedica a la consultoría privada
  en torno al uso de las tecnologías de información y comunicación en
  la educación. Se interesa por proponer, desarrollar, implementar,
  evaluar y reestructurar modelos educativos apoyados por tecnologías
  de interconexión para facilitar la interacción en los procesos
  educativos presenciales, semipresenciales, en línea y a distancia,
  así como en probar metodologías de evaluación apoyadas por
  tecnología de interconexión y comunicación.}{}{México}

\authordata{Sergio Ordóñez}{Doctor y maestro en Economía, graduado en
  las universidades París VII y París VIII, respectivamente. Obtuvo el
  grado de licenciatura en la Universidad Nacional Autónoma de
  México. Forma parte del Sistema Nacional de Investigadores en la
  Unidad de Investigación de Economía del Conocimiento y
  Desarrollo. Investiga temas sobre la economía del conocimiento,
  sector electrónico-informático e industria electrónica.}{}{México}

\authordata{Lila Pagola}{Artista visual y docente en la Universidad
  Nacional de Villa María (Escuela Spilimbergo). Es activista de
  software libre y desde 1999 hasta 2006 coorganizó cada año
  las Jornadas de Artes y Medios Digitales. Como una extensión
  temporal de ese proyecto, en 2003 inició con Laura Benech el
  proyecto \textit{Liminar}, un sitio de difusión sobre arte y
  tecnología orientado a hispanohablantes, en especial al entorno
  latinoamericano y sus intereses.

  Su actuación artística reciente se ubica en el cruce del arte
  público, los medios tácticos y la gestión cultural. En sus trabajos
  más recientes investiga sobre las posibilidades de las obras
  derivadas que permiten las licencias \textit{copyleft} (como
  Creative Commons).}{}{Argentina}

\authordata{Marko Txopitea}{Es ingeniero informático. Estudió en la
  Universidad de Deusto. Trabaja como jefe de proyectos en una firma
  de consultoría informática.

  Colabora en varios proyectos relacionados con el software libre, el
  conocimiento libre y el \textit{hacktivismo}, como Librezale,
  Sindominio, Hackmeeting, Ezebez, Hacktivistas, Indymedia Euskal
  Herria, CompartirEsBueno y otros. Publica artículos sobre
  nuevas tecnologías, internet, redes sociales, propiedad intelectual,
  entre otros temas.}{}{Euskal Herria}

\authordata{Érika Valverde}{Psicóloga social, inició sus estudios en
  la Universidad Centroamericana José Simeón Cañas, El Salvador, y
  finalizó en la Universidad de Costa Rica donde obtuvo el grado de
  licenciatura.

  Desde 2002 está ligada a diferentes proyectos con organizaciones
  sociales centroamericanas, acompañándolas y desarrollando
  metodologías para usos estratégicos de internet, los usos sociales
  de las tecnologías de la información y comunicación (\textsc{tic}),
  y procesos de cambio a software libre. Sus metodologías integran
  temas como alfabetización digital, planificación y facilitación de
  espacios grupales en la red, educación popular, entre otros.

  Desde 2006, ante la necesidad de trabajar con recursos más seguros,
  sostenibles y políticamente congruentes, inició un proceso de
  acercamiento y capacitación en el uso de herramientas de software
  libre y código abierto.}{}{Costa Rica}

\authordata{Werner Westermann}{Docente de Historia y Geografía,
  egresado de la Pontificia Universidad Católica de Chile, donde
  trabaja como investigador; desde 1999 participa de la Red
  Universitaria Nacional (\textsc{reuna}) en investigación y
  desarrollo de proyectos como metodólogo y en el área de diseño
  instuccional en la Videoteca Digital para la Educación Superior
  Alejandría y en la Difusión Multimedial Inalámbrica IP. En 2001
  participa como co-fundador de Ciberanía Consultores, donde
  administra y ejecuta el proyecto fondeado por \textsc{fontec}
  ``Sistema integrado de bienes y servicios para la educación
  primaria''.



  Desde 2004 es consultor en e-learning para el programa
  \textsc{ilpes}, dependiente de la Comisión Económica para América
  Latina y el Caribe (\textsc{eclac-un}).



  Desde 2003, fundador y articulador de la comunidad educativa y
  tecnológica Educalibre (en proceso de incorporarse como
  \textsc{ong}), promoviendo el modelo tecnológico y colaborativo del
  Software Libre en la educación.}{}{Chile}

\authordata{Gunnar Wolf}{Ingeniero en software de formación
  autodidacta; entusiasta, usuario y desarrollador de software libre
  desde 1997, especializado en la administración de redes y en el
  desarrollo de sistemas Web.

  Ha fomentado la cohesión y profesionalización de las comunidades
  locales de software libre. Es fundador del Congreso Nacional de
  Software Libre, y entre 2002 y 2004 se desempeñó como coordinador
  general del mismo. Además, desde 2003 colabora activamente como
  desarrollador del proyecto Debian. Es fundador del Encuentro en
  Línea de Educación, Cultura y Software Libres, mismo que coordinó
  entre 2005 y 2010.

  Desde 2005 trabaja como académico del Instituto de
  Investi\-ga\-ciones Económicas de la \textsc{unam}.}{}{México}
