\chapter{Software libre y construcción democrática de la sociedad}
\chapauth{Alejandro Miranda}{Gunnar Wolf}

\lefthead{\textit{SL} y construcción democrática de la sociedad}
\label{sl_cds}

\introduccion

\noindent  El movimiento del software libre se considera por tradición eminentemente técnico, orientado a la creación de un cuerpo común de conocimiento que se concentra de manera expresa hacia la operación de sistemas de cómputo. Aquí se presenta, en cambio, como uno de los detonantes y uno de los casos más claros de éxito de los movimientos de cultura libre.

Se explora cómo el planteamiento del software libre –nacido como movimiento ideológico a mediados del decenio de 1980–, corresponde con la lógica histórica del desarrollo científico de la humanidad, y se nos presenta como un mecanismo congruente con el mecanismo del desarrollo científico y tecnológico que la ha impulsado en milenios de civilización. Se aborda cómo la propuesta ideológica del software libre se exporta y extiende a otras áreas del conocimiento humano generando una cascada de ideas innovadoras que destacan la producción de conocimiento.

 También se revisan algunos ejemplos, anclados en la sociedad del conocimiento, que reflejan que por medio del software libre se desarrollan características básicas de la libertad en una sociedad moderna y democrática: la confiabilidad, la privacidad, el anonimato y las libertades individuales.

 Hoy en día, hablar del software libre es ya un tema común y, si bien hay muchos puntos que los actores externos no comprenden del todo, las nociones que definen al movimiento en buena medida se han adoptado fuera del medio de los \textit{hackers},\footnote{Cuando un programador –o en general un entusiasta del cómputo– menciona a los \textit{hackers}, no lo hace en el sentido distorsionado y sensacionalista de esta expresión que nos han impuesto diversos medios para calificar a un intruso o delincuente digital. Un \textit{hacker} conoce los secretos de su sistema, y es capaz de programarlo (y goza haciéndolo) para lo que sea que lo requiera \Parencites{8}{19}. } del cual se originó.

 Con este trabajo proyectamos las ideas básicas del movimiento de software libre hacia todos los campos del saber humano, generalizándolo y presentándolo como un modelo mucho más genérico de producción de conocimiento. Analizamos también cómo este modelo influencia otras áreas del saber, y proyectamos el posible efecto en la construcción de una sociedad más democrática, igualitaria e incluyente.





\section{La historia del conocimiento}

\noindent  El conocimiento puede entenderse como ``el estado de quien conoce o sabe algo'' y \textit{conocer} (del latín \textit{cognoscere}) es ``el ejercicio de las facultades intelectuales, la naturaleza, cualidades y relaciones de las cosas'' \Parencite{37}. En la presente obra, de acuerdo con Ordóñez \parencite*{extra9}, el conocimiento consiste en:
\begin{quotation}
  La reproducción en el pensamiento del mundo material, orientada a la transformación (consciente) de la realidad. El conocimiento es, por tanto, indisoluble de la práctica del sujeto social, de la cual constituye simultáneamente una precondición y un resultado, lo que determina la unidad de conocimiento y práctica, es decir: del conocimiento como condición de la práctica, y de la práctica como actividad que genera nuevo conocimiento, el cual a su vez será la condición de una nueva práctica modificada.
\end{quotation}

 Así, el conocimiento y la capacidad de conocer forman parte de la naturaleza humana que puede entenderse como un fenómeno psicológico o de índole social.

 Desde el punto de vista psicológico, el aprendizaje da cuenta del conocer, pero siempre el trabajo cognitivo es el eje y no tenemos evidencia de ese conocimiento hasta que el individuo da muestra conductual de él o decide compartirlo; en este punto, lo aprendido se socializa y le permite a los otros negociar significados y apropiárselo, pasando de la interacción social a la estructura individual. De este modo, el aprendizaje de uno puede dar origen a una idea en otro.

 Durante milenios, la función simbólica del lenguaje (lenguaje oral) representó el medio transmisor de las ideas, que vinculado con el continuo intercambio tecnológico entre los grupos humanos \Parencite{38}, contribuyó a perpetuar las ideas y a generar especializaciones con cada nuevo eslabón. Conocer y distribuir este conocimiento es un comportamiento natural e inherente para la humanidad, similar al efecto de la polinización de las abejas en su actividad cotidiana.

 Con la especialización en la producción de conocimiento, el tiempo que los grupos e individuos podían invertir para generar aportes novedosos se amplió considerablemente. Poco a poco, las sociedades que iban experimentando estos avances generaron reglas para validar y compartir dicho conocimiento, conforme a las cuales ganarían todos los actores involucrados y la humanidad en su conjunto.

 El desarrollo científico es el mejor ejemplo de cómo la humanidad ha logrado acuerdos de procedimientos y formas válidas de retribución en lo relativo a la construcción del conocimiento. A partir de ella surgen los conceptos que hemos heredado de derecho de autor (moral y patrimonial), en un primer momento en que se equiparan los bienes intangibles con los tangibles.

 Al llegar la masificación de la producción científica y literaria mediante la imprenta, resultó natural que los autores cedieran a los impresores algunos privilegios (por ejemplo, la exclusividad sobre una obra), a cambio de recibir otros por parte de ellos (no reproducir una obra más allá de lo pactado y compartir ganancias según lo acordado). Sin embargo, de nuevo con el paso de los siglos, ese pacto y ese modelo han demostrado no alcanzar la velocidad de los cambios tecnológicos que hoy nos rigen.

 Desde hace ya varios decenios, los mimeógrafos y en general las técnicas offset de impresión han puesto la imprenta al alcance de cualquiera. Ya no hace falta una inversión millonaria para montar un taller de impresión, y se reduce el nivel de entrada (y, por tanto, el tributo que el autor debe pagar al prestador del servicio) para quien quiere masificar la distribución de una obra.

 Del mismo modo que la imprenta de tipos móviles se convirtió en uno de los factores detonantes del fin de la Edad Media e inicio del renacimiento, el mimeógrafo permitió difundir las ideas base que llevaron a todas las grandes revoluciones de finales del siglo \textsc{xix} e inicios del \textsc{xx}. No resulta casualidad el que decenas de regímenes autoritarios vieran a estos talleres portátiles de impresión como un peligro y los catalogaran como instrumentos que deberían sujetarse a un estricto control (e incontables confiscaciones).

 Pero apenas hace unos 20 años llegó el último y mayor par\-te\-a\-guas en lo relativo a la masificación: que amplios segmentos de la sociedad tengan acceso a redes de cómputo, con capacidad de reproducir cualquier obra producida por la humanidad, sin degradación de calidad ni inversión de insumos.

 Una interesante consecuencia de la distribución de obras de conocimiento o arte mediante medios digitales es la \textit{granularización} y extensividad de las mismas. La unidad para transmitir de persona a persona deja de ser el libro o el álbum, para convertirse en el artículo o la pieza musical específica. Llega un momento en que la idea vale más que el producto, a pesar de que el medio físico que sustenta a la obra siga teniendo un valor intrínseco.\footnote{Este valor nos explica por qué aún es importante para muchas personas comprar un disco a pesar de tener ya todas sus canciones o un libro que ya habían leído en formato digital. El medio físico sigue atrayéndonos, por ejemplo, gracias a las obras adicionales que éste incluye, por la capacidad de asignarle un peso emocional o sencillamente por la conveniencia de contar con la obra en un medio más cómodo.} En este sentido, muchos productos se desdibujan para convertirse en compilaciones de obras menores.

\vfill

 Además, según la naturaleza de la obra, puede reproducirse sin degradación de calidad y supone la posibilidad de distribuirla agregándole comentarios o corrigiendo diversos aspectos; la digitalización del conocimiento nos lleva a la difusión de obras maleables y extensibles. Más aún, sobre todo en diversas áreas artísticas, se tiene el potencial de incluir directamente una o varias obras como parte de creaciones independientes sin una excesiva inversión de esfuerzo, reinterpretando y expandiendo lo logrado por el autor; por medio de \textit{remixes} (remezclas), \textit{mashups} (integraciones) u otros procesos, con los que se va volviendo difusa la barrera autoral de cada uno de los implicados en agregar valor a una fracción de la obra.

\vfill

 Ese desarrollo nos lleva a la necesidad de romper con el pacto que equiparaba los bienes tangibles con los intangibles, pues para todo propósito éstos han dejado de ser iguales. El conocimiento ya no requiere de inversión para su distribución, y han surgido diversos movimientos –cada uno desde su esquina virtual– que pugnan por devolver el dominio colectivo al conocimiento.

\vfill



\section[El cómputo como expresión del conocimiento]{El cómputo como expresión\\ del conocimiento}

\noindent  Los diferentes grupos que forman parte del movimiento del software libre se autodefinen sobre todo con base en las cuatro libertades, formalizadas en 1986 por la Free Software Foundation \Parencite{9}. Determinado programa puede considerarse software libre en todo el mundo si su licencia de distribución cumple con los siguientes cuatro puntos:

\begin{enumerate}
\item Libertad de uso para cualquier fin, sin restricción.
\item Libertad de aprendizaje.
\item Libertad de modificación y adecuación.
\item Libertad de redistribución.
\end{enumerate}

 Claro está, si bien estos cuatro puntos dan coherencia a un amplio tejido social de movimientos cercanos, en el transcurso de los últimos 20 años han surgido varias corrientes que explicitan varios puntos adicionales \Parencites{10}{11}.

 El software libre no es, como muchos podrían suponer, un invento del decenio de 1980. Durante los primeros decenios del desarrollo del cómputo, prácticamente la totalidad del software disponible era libre, aunque no había conciencia de ello. Era sencillamente lo natural; si bien los individuos o las corporaciones cobraban escrupulosamente el \textit{trabajo invertido} en el desarrollo de soluciones –y cobraban muy bien, dado lo restringido del campo a esas alturas–, el producto resultante (cada uno de los programas) se entregaba junto con el código fuente al contratante de su desarrollo completo. Claro, esto era necesario por la dinámica del cómputo en esa época: si bien un centro universitario, una rama del ejército o una gran empresa podían comprar una computadora de una marca conocida, lo más común es que la modificaran para cumplir con sus necesidades; sí, que modificaran el \textit{hardware} mismo, que le adecuaran interfaces no planeadas en el diseño original, que le conectaran unidades de almacenamiento o despliegue, etcétera.

 Conforme el juego de abstracciones que conforman a un sistema de cómputo se ha tornado más complejo, por el aumento de las capacidades de los equipos, los programadores han requerido menos del contacto con los fierros. Pero sí, en los decenios de 1950 y 1960, una computadora se entregaba completa: código fuente del sistema operativo, esquemas eléctricos de conexiones, todo lo necesario para poder\ldots usarla. Mucha gente dentro del movimiento del software libre se detendría aquí para llamar la atención hacia el hecho de que los usuarios de antaño esperaran como algo natural recibir el código fuente, cuando hoy en día esta afirmación sería risible en cuanto a prácticamente todos los sistemas en el mercado. Nuestra intención, sin embargo, va más allá: tomar al código fuente como una herramienta de expresión entre humanos, y para formalizar y transmitir conocimientos, de manera análoga a la notación utilizada en las matemáticas.

 Phil Salin (\cite*{303}) elaboró por primera vez la noción de que los programas de cómputo constituyen expresiones creativas y, por tanto, deberían ser protegidos por la garantía de libertad de expresión, y éste aún es uno de los principales argumentos de varias de las luchas que en secciones posteriores detallaremos.

 Coleman (\cite*{188}), por su parte, hace referencia a la clara tensión entre los derechos de expresión y la propiedad intelectual. Las leyes de derechos de autor necesariamente limitan la difusión y el uso de las ideas que pertenecen a otros; y aunque el razonamiento detrás de esta censura efectiva indica que es por un bien mayor (el de fomentar una mayor creación por medio de los incentivos económicos), el florecimiento de la producción generada por los movimientos de conocimiento libre demuestra que, al menos con las condiciones favorables a la circulación del conocimiento imperantes el día de hoy, es hora de reevaluar el contrato social que ha permitido etiquetar los bienes intangibles como \textit{propiedad}.

\section{¿Qué es una sociedad democrática?}

\noindent  En este texto, sostenemos que los movimientos relativos a la cultura libre han apuntado históricamente hacia la creación, promoción y desarrollo de una sociedad democrática. Resulta fundamental en este momento definir a qué nos referimos con este término.

 Mucha gente relaciona el concepto de democracia directamente con el de \textit{votaciones} \textit{para elegir la forma de gobierno}, que no es más que una expresión de su significado y posiblemente la más limitada y de estrechas miras. Las sociedades que equiparan democracia con elecciones limitan la acción de la sociedad al ámbito electoral y crean una muy reducida clase política –la única a la que, para efectos prácticos, llama verdaderamente ciudadana– terminan por convertirse en \textit{Estados sin ciudadanos}. Citando a Esteban Castro \parencite*{320}:

\begin{quotation}
  Desde otro ángulo, la evolución a largo plazo de la ciudadanía occidental se ha caracterizado en términos generales por una expansión cualitativa y cuantitativa, aunque este proceso haya sido accidentado y también sujeto a tendencias regresivas. Si hablamos en términos generales, en la época moderna ser un ciudadano evolucionó de ser un ``burgués'' (hombre, jefe de familia y propietario) en las ciudades europeas medievales, a convertirse en un miembro individual (siempre hombre y propietario) de un Estado nación hacia finales del siglo \textsc{xviii}, con la revolución francesa. Posteriormente, durante los siglos \textsc{xix} y \textsc{xx} se desarrollaron formas cada vez más incluyentes de ciudadanía (siempre en el marco del Estado nación), que involucraron la expansión formal de la ciudadanía a las mujeres y a las mayorías no propietarias (aunque siempre se excluyó a amplios sectores de la población, a menudo por motivos étnicos). En tiempos más recientes, hemos sido testigos de la reaparición de formas tradicionales de ciudadanía, así como también del surgimiento de nuevas formas cuya tendencia es trascender las fronteras de los Estados nación, como es el caso de las ciudadanías ``pos-nacional'', ``trasnacional'', ``cosmopolita'', ``mundial'', o ``global''. Por lo tanto, en una perspectiva de largo plazo, puede decirse que como tendencia general, la membresía formal de los sistemas de ciudadanía se ha ido expandiendo hasta incorporar –tomando una frase de Norbert Elias– a ``números siempre crecientes'' de seres humanos.

  [\ldots]

  De manera comprensible, la aplicación mecánica del concepto de ciudadanía a las experiencias de países no europeos es aún más problemática. Por ejemplo, ¿qué quiere decir ser ciudadano en América Latina, o mejor dicho en cada uno de sus países y regiones? Algunos autores han conceptualizado el caso de los países latinoamericanos como una situación de ``Estados sin ciudadanos'', en donde el desarrollo de los Estados nación no tuvo correspondencia con la formación de una ciudadanía que pudiera dar base legítima al ejercicio del poder político. Otros se han referido a la existencia de ``ciudadanos imagi\-narios'', en relación a los limitados intentos, a menudo artificiales, de trasplantar las instituciones de la ciudadanía liberal (particu\-lar\-mente la propiedad privada) a países como México, nación que tenía tradiciones indígenas y españolas muy bien establecidas de propiedad colectiva de los bienes naturales (tierra, agua, bosques).
\end{quotation}



 Por medio de los procesos relativos a la apropiación colectiva del conocimiento que hemos señalado hasta el momento, y mediante los movimientos y valores éticos que describiremos en las siguientes secciones, se crea una \textit{verdadera} vida democrática. Los individuos que han tomado conciencia de estas posibilidades y se han convertido en creadores van forjando sociedades con una elevada dosis de conciencia de participación activa en sus procesos políticos, y que frecuentemente pugnan por impulsar dichos procesos hacia la sociedad general. Una sociedad democrática se apropia de valores como la autorregulación, que define sus propias normas de gestión.

 Mientras que \parencite{320} estudia cómo la capacidad ciudadana del individuo se va reduciendo conforme se van cercando y privatizando los bienes comunes naturales y culturales, nosotros centramos nuestro análisis en el proceso interno: cómo la participación en comunidades de creación colaborativa de conocimiento va generando individuos más participativos de una vida política plena, es decir, individuos que se apropian de los atributos que la ciudadanía ha ido perdiendo en sus países, sin proponérselo explícitamente y mucho menos dando una orientación, aunque aparezca una orientación política emergente en las comunidades \Parencite{188}.

 El enfoque que damos a la \textit{construcción de una sociedad democrática} transita necesariamente por el del involucramiento político, por el de una sociedad participativa, con un significado similar al que \parencite{320} da a \textit{ciudadanía} (las cursivas son nuestras):

\begin{quotation}
  [\ldots] nos aproximamos a la ``ciudadanía'' desde una perspectiva sociológica que enfatiza \textit{el proceso de formación de la ciudadanía} más que la ciudadanía como estatus. Ante todo, ésta es un sistema de inclusión-exclusión que opera siguiendo criterios específicos para definir la \textit{membresía de las personas dentro de una cierta comunidad política, incorporada la asignación de sus derechos y obligaciones}. Dicho proceso es altamente dinámico ya que la ciudadanía se desarrolla con el paso del tiempo en términos cualitativos y cuantitativos. Adopta una diversidad de formas en los distintos territorios, y se caracteriza por las permanentes contradicciones entre el estatus otorgado a las y  los ciudadanos en el plano formal, y el ejercicio efectivo, sustantivo de los derechos y obligaciones que se les permite a los individuos en términos prácticos. En resumen, en este capítulo no estamos enfocando nuestra atención sobre la conexión entre ciudadanía y nacionalidad u otras formas de identidad política, sino más bien abordamos a la ciudadanía como el conjunto de relaciones sociales fundadas en el reconocimiento de los derechos y obligaciones mutuas que caben a los miembros de la sociedad en un plano de igualdad formal, y asimismo, enfatizamos las tensiones que surgen de las contradicciones entre esta igualdad abstracta del estatus formal de la ciudadanía y las asimetrías y desigualdades sociales concretas que caracterizan a los seres humanos reales.
\end{quotation}

 Para fines de nuestro análisis, la democracia conlleva y atraviesa naturalmente los diversos aspectos de autorregulación y gestión, y va de la mano de la participación social en los diferentes esquemas de producción,\footnote{Específicamente en lo tocante a la producción de cultura y conocimiento, mal llamados propiedad  intelectual.} e incluso a la desaparición gradual de la barrera o distinción entre productores y consumidores en este sentido.\footnote{\url{http://seminario.edusol.info/seco3/comentarios/cap_2.html\#res10}}

\section{Del software al conocimiento}

\label{sl_cds:soft_al_conoc}

\noindent  Varios movimientos, de hecho, han surgido basados en conjuntos de premisas similares, y construyen sobre del éxito de la convocatoria que el movimiento ideológico del software libre introdujo.

 Se entiende por \textit{cultura libre} un movimiento social que promueve el desarrollo y progreso de las \textit{obras culturales libres} en un contexto de la sociedad digital y de conocimiento; el movimiento de cultura libre surge en un contexto de leyes de derechos de autor con un énfasis restrictivo, que impiden o limitan ampliamente el desarrollo cultural en ámbitos digitales \Parencite{393}.

 Si bien la construcción de la cultura libre aún está sujeta a interpretaciones de sus límites, hay acuerdos mínimos que permiten hablar de la cultura libre como el libre acceso, creación, modificación, publicación y distribución de todo tipo de obras digitales. \Parencite{394} propone, de forma análoga a las convenciones que definen al software libre:

\begin{enumerate}
\item  \textit{Libertad de usar el trabajo} y disfrutar de los beneficios de su utilización.

\item  \textit{Libertad de estudiar el trabajo} y aplicar el conocimiento adquirido de él.

\item  \textit{Libertad de hacer y redistribuir copias}, totales o parciales, de la información o expresión.

\item  \textit{Libertad de hacer cambios y mejoras}, y distribuir los trabajos derivados.
\end{enumerate}



 Hill y Moeller \parencite*{395} plantean posteriormente una definición oficial y consensuada de las obras culturales libres. Hay una gran variedad de grupos que se identifican con la cultura libre, por lo cual nos limitaremos a mencionar algunos ejemplos sobresalientes.

 \textit{Wikipedia} surgió con una idea que no es novedosa; ser una colección del conocimiento humano. Ya la biblioteca de Alejandría o los enciclopedistas del siglo \textsc{xviii} lo habían intentado; el antecedente inmediato de Wikipedia fue \textit{Nupedia} \Parencite{304}, que operó de marzo de 2000 a septiembre 2003 como una enciclopedia de libre acceso y redistribución que garantizaba la calidad de sus contenidos por medio de la revisión por pares. En los primeros 18 meses sólo se publicaron 20 artículos. En la búsqueda de nuevas fórmulas para involucrar a más personas en la producción de contenidos, se pensó que los usuarios de \textit{Nupedia} crearan los contenidos que luego los editores y expertos revisarían.

 Desde sus inicios, Wikipedia rebasó con rapidez la velocidad de producción de \textit{Nupedia}, dejándola con el tiempo inoperante. En la edición en inglés, durante el primer mes reunió 1~000 artículos; el primer año, 20~000; el segundo año, 100~000, y al día de hoy sobrepasa los 3~300~000. Del mismo modo, no tardó en arrancar en otros idiomas y actualmente hay más de 30 lenguajes de Wikipedia con más de 100~000 artículos \Parencite{743}.

 \textit{Wikipedia }generó con su éxito una gran revolución: de la idea de compilar el conocimiento revisado por pares expertos y disponible bajo una licencia de uso que garantizara su libre redistribución, pasó a una comunidad compuesta por visitantes, usuarios que contribuyen, bibliotecarios, burócratas y un comité directivo organizado en torno a una sociedad sin fines de lucro que se encarga del financiamiento del proyecto y la organización del encuentro anual de \textit{wikipedistas} \Parencite{36}.

 Como todas las comunidades que van construyendo su acervo documental basados en el perfeccionamiento reiterativo y colectivo, \textit{Wikipedia }constituye un claro ejemplo de que \textit{ningún conocimiento es acabado}.\footnote{\url{http://seminario.edusol.info/seco3/comentarios/cap_2.html\#res3}}

 Pero no sólo el conocimiento formalizado puede compartirse. En 2001 nació Creative Commons (\textsc{cc}), impulsada por el abogado estadounidense Larry Lessig. Esta organización liderada localmente en una gran cantidad de países por personalidades versadas en temas legales, fue creada para servir como punto de referencia para quien quiera crear obras artísticas, intelectuales y científicas libres. Asimismo, ofrece un marco legal para que gente no experta en estos temas pueda elegir los términos de licenciamiento que juzgue más adecuados para su creación, sin tener que ahondar de más en las áridas estepas legales; se mantiene asesorada y liderada por un grupo de abogados, cuya principal labor es traducir y adecuar las licencias base de \textsc{cc} para cada una de las jurisdicciones en que sean aplicables. Alrededor de este modelo ha surgido un grupo de creadores, y una gran cantidad de sitios de alto perfil en la red han acogido su propuesta. Si bien no todas las licencias de \textsc{cc} califican como cultura libre, algunas que claramente sí lo son han ayudado fuertemente a llevar estas ideas a la conciencia general.

\section{El software libre en tanto movimiento social}

\noindent  Los seguidores de una ideología determinada no suelen limitarse a impulsarla en el marco original estricto en que se conformó, sino que exportan sus ideas a otros campos, en principio relacionados muy de cerca, y cada vez abarcan un mayor territorio.

 Algunos encuentran difícil de entender, incluso ven como un contrasentido, que los promotores del movimiento del software libre siempre se vinculen con la defensa de la privacidad y el anonimato. Estos dos puntos, si bien muy distintos, van claramente de la mano y resultan del interés de usuarios y desarrolladores por dos motivos principales: el reto técnico y el posicionamiento ético-ideológico. Ilustraremos, pues, el engranaje entre ambos aspectos en la presente sección.

 La gente que más se identifica con los principios del movimiento del software libre tiende también a sentir una fuerte identificación con los movimientos que defienden las libertades individuales, lo cual se nota en primer término en lo relativo a las libertades en línea, pero llega a ámbitos muy variados.

 Puede parecer contradictorio que la misma gente que aboga por la libertad y el acceso universal al código sea la misma que con más vehemencia lucha por mantener –técnica y legalmente– el derecho a la privacidad y a la confidencialidad respecto a los datos y actividades personales. Tal vez los dos referentes más importantes en este rubro sean el Chaos Computer Club \Parencite{27}, organización alemana fundada en 1981 que se autodescribe como una \textit{comunidad galáctica de seres vivos, independientemente de su edad, sexo, raza u orientación social, que lucha a través de las fronteras en pro de la libertad de la información}, y la Electronic Frontier Foundation \Parencite{28}, organización estadounidense que ha prestado asesoría o representación legal de diferentes maneras en casos relacionados con las libertades individuales en un mundo digital. Ambas organizaciones nacieron mucho antes de que los retos que enfrentan se hicieran evidentes a la sociedad en su conjunto. Las investigaciones y las batallas legales que han librado durante decenios son, sin exagerar, causantes directas de que hoy en día tengamos libertad de expresión y acceso a la comunicación privada en internet \Parencite{29}.

 Hay una amplia gama de luchas que podrían parecer completamente laterales a las características sustanciales con las que se ha identificado a los grupos de desarrollo de software libre, pero con frecuencia tienen importantes implicaciones sociales o políticas por relacionarse con la interpretación de diversas actividades en el ámbito de los derechos civiles, como el derecho a la privacidad o la libertad de expresión.

 Un punto interesante de esas luchas es la manera en que el razonamiento relativo suele presentarse: a partir de puntos de vista más cercanos a la lógica formal que a la lógica política que es común en muchos otros ámbitos. Esta característica ha llevado a resultados tan contundentes y sorpresivos como el caso de Bernstein contra el Departamento de Justicia de Estados Unidos \Parencite{328}, que dictaminó que el código es expresión de la creatividad humana y, por tanto, debe gozar de la protección de las garantías de libertad de expresión.

 Pero la identificación llega también por el otro lado. Prominentes organizaciones de la sociedad civil han hallado su afinidad y la congruencia de sus objetivos con los diversos movimientos aquí mencionados. Organizaciones de todos los ángulos del espectro social han encontrado que la única manera de concentrarse en cubrir cabalmente sus objetivos es mediante las diversas expresiones del conocimiento libre, y una gran cantidad de elementos en común con el movimiento del software libre los ha llevado a combinar sus experiencias y métodos, de modo que hibridizan los movimientos y crean una muy interesante sinergia.

 El movimiento de software libre, argumenta Coleman \Parencite{188}, es \textit{políticamente agnóstico} y hay una relación no expresa y asimétrica entre el del software libre y diversos movimientos políticos:

\begin{quotation}
  Es posible sospechar que el \textsc{foss}\footnote{\textit{Free and Open Source Software}, una manera de referirse a los diferentes submovimientos de software libre (open Source) agrupándolos y obviando sus diferencias internas (nota del traductor).} tiene una agenda política deliberada; pero si se les pregunta, los desarrolladores de \textsc{foss} invariablemente responden con un ``no'' firme e inambiguo, típi\-camente seguido de un léxico preciso discutiendo la relación correcta entre el \textsc{foss} y la política. Por ejemplo, si bien es perfectamente aceptable tener un panel relativo al software libre en un congreso antiglobalización, los desarrolladores de \textsc{foss} sugerirían que es inaceptable implicar que el \textsc{foss} tenga a la antiglobalización como uno de sus objetivos, o a cualquier otro programa político. Una diferencia sutil pero vital.

  [\ldots] Descrito simplemente, los reclamos políticos más allá del software limitan, ensucian y censuran la esfera de circulación de pensamiento, acción y expresión. Se siente que si el \textsc{foss} se dirige con fines políticos, contaminará la ``pureza'' del proceso de toma de decisiones. La afiliación política puede también desmotivar a algunas personas de participar en el desarrollo, creando una barrera artificial para entrar a esta esfera.
\end{quotation}



 Sin embargo, es imposible disociar por completo la política de cualquier movimiento social. De acuerdo con el comentario de Ca\-ro\-li\-na Franco al presente artículo, todo movimiento social es inherentemente político, aunque esto se expresa en diversos vectores que muchas veces se diferencian con claridad de la política partidaria tradicional. Según la taxonomía presentada en Wikipedia \parencite*{601}, basada en Aberle \parencite*{602}, Franco define el software libre como \textit{un movimiento de reforma con alcance tanto grupal como individual, que utiliza métodos pacíficos de trabajo, y de rango global. ¿Por qué un movimiento de reforma y no radical? Porque busca cambiar algunas cosas en específico, que implican limitaciones}.

 Ejemplos de movimientos y organizaciones preexistentes que han encontrado alianzas naturales con el software libre en tanto movimiento son:

\begin{itemize}
\item  \textit{Indymedia} \Parencite{325}. Se autodefine como \textit{un colectivo de organizaciones de medios independientes y de cientos de periodistas que ofrecen una cobertura de base y no comercial. Indymedia es una vía democrática de medios de comunicación para la creación radical de narraciones verídicas y apasionadas}. \textit{Indymedia} cuenta con el apoyo y la cercanía de diversos miembros de grupos de software libre para la creación y administración del sistema de administración de contenido Mir \Parencite{326}, a la medida de las necesidades particulares del flujo de información correspondiente a los centros de medios de comunicación independientes (\textsc{cmci}), y se utiliza como base para más de 30 de éstos.

\item  El gobierno de la Comunidad Autónoma de Extremadura, España. Esta administración puso en marcha en 2002 el ambicioso proyecto de Linux Extremeño, o gnuLinEx \Parencite{327}. Por medio de una distribución de \textsc{gnu}/Linux basada en Debian y preparada expresamente con las necesidades de la Comunidad Autónoma en mente, se lograron ahorros de licenciamiento tan amplios que posibilitaron la dotación a las escuelas de una computadora por cada dos niños en edad escolar; además, gnuLinEx dio el gran salto para convertirse de consumidor de tecnología en un fuerte promotor del desarrollo, al crear una plantilla de desarrolladores dedicados a mantener la distribución al día, y contribuir de vuelta a los proyectos padre, sobre todo Debian. La Junta de Extremadura ha patrocinado una gran cantidad de reuniones de desarrollo, generales y focalizadas, en el transcurso de todos estos años; por ejemplo, efectuó una importante inversión que permitió la celebración en julio de 2009 del Congreso anual de desarrolladores de Debian (DebConf).

\item  La Fundación Heinrich Böll \Parencite{600}. Se constituyó en Colonia, Alemania, en 1986, en el seno del partido alemán Alianza 90/Los Verdes, y ha ampliado su ámbito de acción a diversos países de América Latina. Centra su actividad en cuatro líneas: ecología y desarrollo sostenible; derechos de la mujer y democracia de género; democracia y derechos humanos, y libertad de prensa y crítica pública. Organiza diversas actividades académicas y culturales, y constituye una prolífica editorial en estos temas; desde hace varios años se ha acercado a diversos activistas del software libre, dado que este movimiento aborda temas transversales a muchos de los que desarrolla la fundación, y ha publicado varios libros colectivos y multidisciplinarios en los cuales el punto de vista de las comunidades de software libre tiene un peso importante.
\end{itemize}



 Ejemplos como los anteriores, claro está, hay muchos más, y éstos deben verse sólo como una pequeña muestra. A lo largo del presente libro –muy particularmente en los tres primeros capítulos de la tercera sección, así como en los apéndices B, C y D– veremos ejemplificar cómo diversos participantes del movimiento del software libre exponen el significado social del movimiento ante una sociedad menos sensibilizada a los aspectos técnicos, tanto desde un punto ideológico como de implementación técnica.

\section{Conclusiones}

\noindent  En este tema incluimos algunos ejemplos que destacan la importancia de la libertad del conocimiento, ya no sólo como una ventaja para el individuo o como una metodología de desarrollo, sino como un eslabón natural del proceso de descubrimiento, adecuación, descripción y modificación del entorno que la humanidad ha realizado desde su aparición en la Tierra. Además, expusimos con un par de ejemplos el porqué el movimiento del \textit{software }libre está firmemente basado en valores éticos y políticos que han creado una cultura sobre la forma de compartir el conocimiento.

 Este movimiento, en particular en los últimos años, ha sido objeto de análisis por parte de especialistas de diferentes ramas del conocimiento humano, pero sobre todo ha ido ganando simpatizantes que han ampliado las nociones de libertad a sus distintas áreas disciplinarias. El efecto sobre la producción de la humanidad a futuro apenas comienza a vislumbrarse.
