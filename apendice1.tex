\chapter{Educación y software libre}
\chapauth{Alejandro Miranda}

\lefthead{Educación y software libre}

\noindent  Hablar de la educación con software libre implica en primera instancia mencionar la educación y la tecnología, que tradicionalmente ha caminado en medio de la pugna de al menos dos líneas de pensamiento: aquellas que por lo general predominan en el uso del cómputo en la educación y las que reestructuran la apropiación del cómputo como una herramienta que potencia la actividad humana.

 En la primera línea, Collins \parencite*{838} indica los siguientes usos más comunes del cómputo en la educación:

\begin{description}
\item[Para llevar a cabo tareas.] Uso de procesadores de texto, hojas de cálculo, herramientas de dibujo, herramientas para presentaciones, lenguajes de programación, entre muchas herramientas con una infinidad de aplicaciones.

\item[Sistemas integrados del aprendizaje.] Éstos integran actividades de aprendizaje (generalmente en solitario) con un registro que sirve de referente para el docente, la administración y el alumno.

\item[Simuladores y juegos.] Diseñados normalmente como un ejercicio lúdico pero educativo.

\item[Redes de comunicación.] Los profesores y estudiantes interactúan por medio de las herramientas de interconexión como las páginas web dinámicas, el correo electrónico, los foros en web y las bases de datos.

\item[Entornos de aprendizaje interactivos.] El estudiante tiene un papel activo, que simula el ejercicio en una profesión u oficio, y a medida que va realizando las tareas que el sistema le propone, obtiene retroalimentación a su desempeño.
\end{description}



 En la segunda, el cómputo y las telecomunicaciones han potenciado cambios importantes en la educación, como el crecimiento de una cultura de la interacción y el diálogo, lo cual sembró de manera paradójica algunos problemas que la educación y, en particular, la escuela afrontan a pesar de los amplios y reiterados intentos por apropiárselos e incorporarlos. Por ejemplo, Tapscott \parencite*{847} reporta que, desde los setenta, la instrucción asistida por computadora (\textsc{cai}, por sus siglas en inglés) representó una propuesta interactiva novedosa que prometía responder a las características personales de los alumnos, pero no se dieron las condiciones para su permanencia. Posteriormente, a finales de los noventa, la interactividad se reelabora con internet y florecen herramientas que permiten mayor flexibilidad en la interacción usuario-usuario y usuario-máquina.

 Algunos cambios que prometen perpetuidad en este nuevo pa\-ra\-dig\-ma mediático, frente al modelo ``tradicional'', incluyen:

\begin{enumerate}
\item Romper con la linealidad textual para pasar al salto hipermedial, lo cual implica una nueva forma, cognitiva, de acercarse al co\-no\-ci\-mien\-to explícito.

\item De este primer cambio en la forma de aproximarse al co\-no\-ci\-mien\-to, se deriva otro igual de importante: se pasa de la instrucción a la construcción, al fomentar el aprendizaje por descubrimiento, que si bien no constituyen nociones que se originan desde la educación y el cómputo, se han facilitado e impulsado por la mediación que proporcionan estas tecnologías.

\item Si uno de los cambios de este paradigma es la pérdida de la fuerza de la instrucción, el centro pasa del profesor al estudiante, cualidad multipromocionada por las teorías socio\-cons\-truc\-ti\-vistas.

\item Cuando el centro es el estudiante, el eje principal pasa de la enseñanza al aprendizaje, lo que plantea como panorama la posibilidad del mal llamado ``aprendizaje a lo largo de la vida'', expresión que implica desconocer que aprendemos de todo y todo el tiempo, cuando en realidad se desea hacer referencia a que la escuela pierde predominio como espacio para el aprendizaje y los aprendices ahora pueden asumir su propia guía para enfrentar nuevos dominios de conocimiento dentro y fuera del contexto escolar.

\item Sin el predominio del arreglo escolar, entonces la idea de un dominio único de conocimiento al que un grupo de aprendices debía aproximarse es reemplazada por la enseñanza personalizada, ya sea desde la mediación de un agente computacional o, mejor, un guía o tutor, hasta las propuesta más individualistas como el \textit{edupunk}, que recupera el \textit{hágalo usted mismo} y además mantiene un estrecho vínculo con la propuesta de aprendizaje \textit{hacker} que revisaremos más adelante.

\item Esta idea del \textit{hágalo usted mismo} lleva como fondo un concepto importante en el cambio de paradigma: la motivación, la enseñanza y el aprendizaje ya no se centran en el eje extrínseco e institucional; se guía por los intereses particulares del aprendiz y va desvaneciendo la idea de la enseñanza como una tortura para acercarse al aprendizaje como una actividad lúdica.

  Este último punto será relevante para comprender el porqué de los factores motivacionales que sustentan el aprendizaje no formal de las comunidades de software libre.

\item Como lo hemos planteado, en este nuevo panorama educativo con el cómputo y las tecnologías como trasfondo (aunque no necesariamente son una prerrecurrente), el profesor debe transformase en guía o facilitador de la apropiación de nuevos campos de conocimiento (argumento sobreutilizado en las propuestas constructivistas) y en algunos escenarios se plantea la ausencia de una guía formal.
\end{enumerate}

 De vuelta al planteamiento inicial, en el caso del software libre. La primera línea de pensamiento se representa por el uso de la tecnología en la educación como un fin en sí mismo, común en espacios donde el cómputo es objeto de estudio o donde se predomina el equipo sobre el diseño educativo. En el segundo eje, encontramos un planteamiento centrado en la actividad humana que enfatiza la tradición en los estilos del ``aprendizaje \textit{hacker}'', el \textit{hágalo usted mismo} y los ejes éticos de la comunidad dominantes en la comunidad de software libre.

\section{Eje tecnológico}

\subsection{¿Software libre en la educación\\ o educación con software libre?}

\noindent  El software libre nació en los centros de investigación y se insertó poco a poco en las empresas, universidades y escuelas tecnológicas en las que sus simpatizantes, promotores y desarrolladores encontraron terreno fértil para divulgar y sumar esfuerzos en torno a las cuatro libertades dominantes propuestas originalmente por la Free Software Foundation alrededor de las cuales han convergido casi todas las autodefiniciones de comunidades vinculadas a la creación de software libre\footnote{Cabe destacar que no son el único punto de vista en lo que respecta a la definición de la libertad del software; por ejemplo, el proyecto Debian indica en su contrato social que el software libre debe cumplir con las siguientes características: permitir su libre redistribución; no prohibir los trabajos derivados y su distribución bajo los mismos términos que el original; pueden exigir la integridad del código fuente siempre y cuando permitan la distribución de los respectivos parches; el licenciamiento no debe discriminar personas o grupos; no se puede restringir el uso de la herramienta para una finalidad determinada; el licenciamiento del programa debe aplicarse por igual a todos los que se les redistribuya (relacionado directamente con los puntos de no discriminar grupos y la restricción de uso, es decir, todos los que acceden al programa, así como a su código fuente, deben tener los mismos derechos); y por último el licenciamiento del código no debe restringir a otros programas que se distribuyan junto con él.} en las comunidades \Parencites{839}{849}{859}.

\begin{enumerate}
\item  La libertad de usar el programa, con cualquier propósito.

\item  La libertad de estudiar y adaptar el programa.

\item  La libertad de distribuir copias.

\item  La libertad de mejorar el programa y hacer públicas las mejoras.
\end{enumerate}



 Estos principios éticos enmarcan y definen al software libre, creando un contexto de construcción colaborativa, que en principio se dirige al desarrollo de software y posteriormente se deriva a otros tipos de conocimiento, que abordaremos en el apartado del eje ético y cognitivo.

 Así pues, cuando en estas comunidades se habla de software libre y educación, se entiende sobre todo el ejercicio de la segunda libertad que hace referencia a la capacidad de estudiar y adaptar el programa, eje que enfatiza la actividad primaria del grupo y que da sentido a la acción del sistema completo. Conforme a este crecimiento orgánico, los contextos comúnmente técnicos y universitarios se observan como un espacio natural de crecimiento del uso del software libre.

 El siguiente punto de referencia al tema educativo, desde el eje tecnológico, sucede cuando se crea un grupo de ecosistemas comunitarios que giran en torno a la construcción y mantenimiento de programas (que como hemos visto supone desarrollo de código) dedicados a facilitar la gestión docente, la enseñanza o el aprendizaje y hasta la administración escolar. Aquí podemos incluir múltiples programas de ``corte educativo'':

\begin{itemize}
\item  Los sistemas de administración del aprendizaje (\textsc{lms}, por sus siglas en inglés).

\item  Los portafolios electrónicos y ambientes personales.

\item  Los de la administración escolar.

\item  Los educativos con fines específicos.

\item  Los educativos con fines generales, como los paquetes ofimáticos.
\end{itemize}



 Hay muchos otros paquetes informáticos aplicables en un contexto de enseñanza-aprendizaje y donde la tarea primaria no es la enseñanza o el aprendizaje, sino satisfacer una necesidad por medio del desarrollo de código. Es común encontrar una infinidad de programas que prometen facilitar algún fenómeno relacionado con la educación, pero sin el fundamento preciso para demostrarlo; esto es comprensible desde la perspectiva que le da origen, ya que no se le concede importancia a la comprobación empírica de que se da una solución óptima al fenómeno; lo relevante es cubrir el espacio que origina una necesidad, por medio del desarrollo de código.

 Un nivel más, emparentado con el punto anterior, lo encontramos en el ``empaquetamiento'' de una ``distribución educativa'', entendida como una colección de paquetes preseleccionados y preconfigurados por considerarse útiles en el contexto educativo.

 El amplio universo provee bastantes ejemplos, entre otros: \href{http://www.guadalinex.org}{Guadalinex (Gobierno Autonómico de Andalucía)}, \href{http://www.edulinux.cl/}{EduLinux (Ministerio de Educación de Chile),} \href{http://abuledu.org/}{AbulÉdu (distribución francesa),} \href{http://www.edulibreos.com/}{EdulibreOs (distribución Guatemalteca),} \href{http://lliurex.net/home/}{LliureX (Consejería de Educación de la Generalidad Valenciana),} \href{ http://www.educa2.madrid.org/web/max}{\textsc{max} (Consejería de Educación de la Comunidad de Madrid),} \href{http://wiki.debian.org/DebianEdu}{DebianEdu (colección de paquetes educativos de la distribución Linux Debian)} y \href{http://edubuntu.org/}{Edubuntu (derivación oficial de la distribución Linux Ubuntu).} Es importante resaltar que algunas de estas distribuciones no sólo buscan el empaquetamiento de los programas, sino explotar o detonar una identidad regional por medio de la distribución.

 Puede sostenerse que toda distribución se realiza para ser instalada, algunas desde contextos institucionales como en el caso de los gobiernos autónomos españoles o bien desde contextos más orgánicos como los grupos de usuarios de Linux (\textsc{lug}, por sus siglas en inglés) o asociaciones civiles dedicadas al tema. Pero todas tienen el estrecho vínculo entre las distribuciones educativas y la búsqueda del uso del software libre en el contexto educativo, con algún fin como el económico, el de la libertad y hasta el político-electoral.

 Hay un último elemento por distinguir en la compleja ecología del código frente a la educación. Me refiero a los que desde una lógica económica han desarrollado estrategias de reúso de equipos de desecho para el aprovechamiento con fines educativos, los denominados clientes ligeros que cuentan con amplia popularidad entre los activistas tecnólogos dedicados al tema de la educación con software libre.

 Todo ese conjunto de elementos conforma lo que he denominado el \textit{softwarelibrecentrismo} de las comunidades, que desde la frontera del eje tecnológico se refieren a la educación, así que cuando se piensa en la educación con software libre en general limitan las posibilidades de intervención educativa a desterrar un monopolio\footnote{Comúnmente encarnado en Windows.} e implantar otro que dice garantizar el libre intercambio de la información y conocimiento, aunque sólo se acote a la creación, mantenimiento, instalación y uso del software.

\subsection{Argumentos del eje tecnológico,\\ de la ingenuidad educativa}

\noindent  Los argumentos tradicionales a favor del uso del software libre en la educación abarcan:

\begin{itemize}
\item  La formación de profesionales y estudiantes que se apropian del uso ``genérico'' de herramientas, mientras que lo imperante hoy en día es la enseñanza de software propietario de marcas específicas. Esta afirmación sólo es cierta en el caso del uso del software libre como alternativa tecnológica, porque el uso exclusivo de software libre en la educación también desemboca en la enseñanza exclusiva; la capacidad de transferencia se aumenta con la alternancia entre programas sean libres o privativos.\footnote{Concepto usado por la comunidad para referirse a los programas no libres.} Otro argumento relacionado, que suele encadenarse al tema de la apropiación del software libre, es que se da un impulso al desarrollo de profesionales altamente capacitados que estarían en posición de dar soporte (local e internacional) y colaborar en el desarrollo de código para las herramientas comunes en estos contextos.

\item  La reducción de costos, que va desde el ahorro en el licenciamiento de múltiples programas equivalentes,\footnote{El tema del ahorro es un punto delicado en tanto muchos tienden a confundir la libertad del software, representadas por medio de las cuatro libertades dominantes, con la gratuidad. El software libre tiene costos; la mayor parte de las veces es patrocinado por una entidad que cubre los gastos para un tercero, y en otras ocasiones es adquirido por parte del usuario normalmente a costos menores que los equivalentes privativos.} hasta el reúso de equipos de cómputo aparentemente obsoletos y de desecho. Sobra decir que este argumento es muy atractivo para la mayoría de instituciones educativas, en tanto que permite redirigir los fondos a otros espacios.

\item  La posibilidad legal de copiar, lo que permite a los estudiantes instalar en otros contextos el software que usan en los espacios escolares. Asociado a este argumento se suma, en los espacios de la enseñanza de la informática y áreas afines, la posibilidad de acceder al código y modificarlo, por lo que los estudiantes no sólo encuentran el uso en la escuela, casa y trabajo; también podrían ver cómo se constituye y adapta, e incluso contribuir con reportes, parches y mejoras.

\item  El último argumento que parece el más débil para una institución educativa es la posibilidad del control del software (al auditarlo y controlar su calidad); con el acceso al código cabe esa posibilidad, pero se requiere personal con la experiencia necesaria para ejecutar la tarea.
\end{itemize}



 Desde un particular punto de vista, algunas líneas argumentativas pierden sentido fuera del contexto de la enseñanza de software y ejes asociados como la programación en el ámbito educativo. El acento en la posibilidad de aprender por medio del acceso y modificación del código, fuera de los espacios escolares donde la computación es el objeto de estudio, carece de sentido; porque aquello que parece primordial en un plano resulta irrelevante en otro contexto.

 De poco valen todos los esfuerzos invertidos por activistas del software libre que destacan las libertades del código, ya que del lado educativo el cómputo representa una herramienta para alcanzar un fin y no el fin en sí mismo.

 El acento en la sustitución de software privativo por libre, si bien éticamente es sostenible y comprensible, dentro de una escuela lleva el riesgo de presumir que la sola introducción de programas libres han de gestar cambios éticos, cognitivos y de comportamiento en los aprendices, por ejemplo, ¿acaso el uso de estas herramientas libres garantiza la libre expresión? De igual modo, si la sustitución de programas privativos por libres no se acompaña de la comprensión y apropiación de la propuesta ética, en el mejor de los casos se reduce al uso de herramientas ``gratuitas'', típicamente dejando latente la posibilidad de ejercer plenamente las libertades del software libre.

 En el mismo sentido de aquellas ilusiones provenientes de la tecnología educativa, representada en sus diversas etapas de desarrollo con la introducción de la radio, la televisión o la educación en línea, en las que se posaron las esperanzas de la excelencia educativa; se ha dicho que el eje tecnológico de la educación con software libre, implementado por medio de la instalación de distribuciones ``educativas'', mejorará el contexto educativo, como si el acto educativo se destilara y añejara en los paquetes, para ofrecer con su solo uso cambios significativos en la educación; parece que pecan de ingenuidad educativa al perder de vista que las herramientas computaciones son únicamente un medio para lograr cierto fin educativo.

 Con estos tres últimos argumentos deseo resaltar que un segmento importante de la comunidad de software libre impulsa propuestas con efecto educativo desde un profundo \textit{softwarelibrecentrismo} que le impiden apreciar otras variables relevantes del fenómeno. Este segmento de activistas habla del software libre en la educación y no de una educación con software libre.

\section{Eje ético y cognitivo, el software libre como herramienta\\ del pensamiento humano}

\subsection{De vuelta a lo importante, la educación}

\noindent  Y cuando el \textit{sofwarelibrecentrismo} parecía dominar la escena, desde el núcleo mismo de la Free Software Fundation, se promueve una reconceptualización de lo que a su entendimiento debe ser la educación con software libre, Stallman \parencite*{846}, en su artículo ``Why schools should exclusively use free software'', expone tres puntos con los que se cambian los ejes argumentativos de las comunidades de software libre en la educación:

\begin{description}
\item[Ahorro.] Argumento importante, pero superficial; fácilmente alcanzable por empresas de programas privativos.

\item[Acceso al código.] Desde mi parecer lo menos relevante, como ya lo he explicado, aunque este nuevo giro lo hace atractivo para los no tecnólogos; el acceso al conocimiento, como parte importante de esta sociedad que convive y procesa por medio del intercambio de conocimiento.

\item[Compartir.] En este caso software, pero se apuesta a que puede ampliarse y encadenarse a otras áreas dando ``una lección cívica llevada a la práctica''.
\end{description}



 Los últimos dos puntos destacan los estilos de vida para una sociedad en convivencia, el eje ético del software libre que la escuela puede aprovechar. El cambio de enfoque es relevante si consideramos que el tema de la ética, la convivencia y la educación cívica han ido en aumento en los últimos años en el discurso educativo; hoy en día es común hablar del clima escolar, de la apropiación de formas de convivencia, del respeto al otro y en este sentido el software libre puede convertirse en un ejemplo ``vivo'' de compartir y colaborar alrededor de conceptos tan abstractos como el conocimiento.

\subsection{Educación, camino a los bienes comunes}

\noindent  El cambio en el enfoque de la Free Software Foundation, de la defensa exclusiva de la libertad de código al de resaltar las bondades éticas para transitar a una sociedad en convivencia, puede ser fácilmente apuntalado con otras bondades éticas del contrato social del proyecto Debian, como el destierro de las actitudes discriminatorias (mediante el licenciamiento); esto ya coloca al tema de la educación con software libre en el enfoque del análisis de la actividad humana.

 Quiere decir que en vez de considerar al código y su libertad como el eje que le da inclusión a la educación se propone a la actividad de las comunidades de software libre como un modelo susceptible de exportación a los espacios educativos.

 Tomar las comunidades de software libre como un modelo no se encuentra lejos de lo que plantea la teoría educativa, por ejemplo, en internet se conforman grupos con una actividad constante y en algunos casos por la afiliación voluntaria se constituyen en comunidades de aprendizaje o de práctica.\footnote{Las de software libre cumplen con todos los criterios teóricos para considerarse comunidades de práctica.}

 De hecho esos practicantes han aportado muchos de los elementos que constituyen los cimientos de la floreciente internet. Para Rheingold \parencite*{845}, las comunidades de práctica gestaron internet como el bien común con mayor éxito contemporáneo, en el que la infraestructura física es sólo una parte de la fórmula que incluye nuevos contratos sociales y posibilita la creación y mantenimiento de una fuente común de conocimiento. Este contrato social se origina en una muy temprana edad del desarrollo de la tecnología de los microprocesadores e internet, cuando los desarrolladores de sistemas llamados a sí mismos \textit{hackers} ``crearon un recurso que beneficiase a todo el mundo, empezando por los propios colaboradores que lo crearon'' y simultáneamente evitaron obstáculos que más adelante imposibilitarían la progresión tecnológica de los desarrollos futuros. Estos \textit{hackers} crearon un nuevo producto que es un bien común resultado de la propiedad intelectual.

\begin{quotation}
  A diferencia del uso popular del término \textit{hacker} como equivalente a delincuente informático, los \textit{hackers} han desarrollado conocimiento sobre tecnología basados en el trabajo colectivo, en la motivación de resolver problemas y construir propuestas novedosas mientras defienden la libertad de la información. Dentro de la cultura \textit{hacker} se define a los delincuentes informáticos como \textit{crackers}, aunque hay términos específicos que categorizan los subtipos de \textit{hackers} y \textit{crackers} \Parencite{850}.

 El comportamiento \textit{hacker} se encuentra estrechamente vinculado a postulados éticos ampliamente descritos por primera vez en el libro de Levy \parencite*{851}, \textit{Hackers: héroes de la revolución computacional}. Estos postulados pueden resumiese en las siguientes premisas:

\begin{enumerate}
\item El acceso a las computadoras y cualquier cosa que pueda enseñar algo sobre la forma en que funciona el mundo debe ser ilimitado y total.

\item Toda la información debe ser libre.

\item Desconfiar de la autoridad promoviendo la descentralización.

\item Los \textit{hackers} deben ser juzgados por su labor, y no por falsos criterios como títulos, edad, raza o posición.

\item Puedes crear arte y belleza en una computadora.

\item Las computadoras pueden cambiar tu vida para mejorarla.
\end{enumerate}
\end{quotation}

 Si bien por sus características de amplitud es difícil definir los bienes comunes \Parencite[50]{848} nos indican que una definición genérica e inicial puede ser la que:

\begin{quotation}
  [\ldots] remite a caracterizar como tales aquellos bienes que se producen, se heredan o transmiten en una situación de comunidad. Son bienes que pertenecen y responden al interés de todos y cada uno de los integrantes de una comunidad. Son bienes que redundan en beneficio o perjuicio de todos y cada uno de estos miembros o ciudadanos por su condición de tal.
\end{quotation}

 En opinión de dichos autores, las tecnologías digitales han cambiado profundamente la forma de conceptualizar los bienes comunes; ya no sólo se trata de gestionar y definir cómo debe ser el acceso y administración de los bienes materiales. Con las tecnologías \Parencite[61]{848}:

\begin{quotation}
  [\ldots] se articulan en artefactos complejos [\ldots] (que) son el centro de permanentes negociaciones entre diferentes grupos de actores que buscan alinear intereses y un mayor control sobre todo tipo de artefactos, conductas y espacios
\end{quotation}

 La propiedad común de esos bienes intangibles son resultado de la acción y negociación de los actores y sus comunidades que generan una construcción conjunta en la que se vincula el plano jurídico-político y el tecnológico, que se construyen continuamente.

 Habrá que agregar que los bienes comunes digitales se hacen tangibles por medio de la acción y la práctica, al ejercitar una actividad de práctica concreta que termina moldeando nuevas formas de cooperar, colaborar y aprender con el uso de las tecnologías; se trata de una construcción cultural de los nuevos bienes comunes digitales.

 Ostrom \parencite*{844} señala que la teoría clásica de los bienes comunes indica que los usuarios dependen de un macrosistema; pero, cuando tienen la posibilidad de organizarse, interactuar y generar confianza, dependen de los atributos y recursos del sistema frente a los beneficios por conseguir contra los costos para obtenerlos. Estos nuevos sistemas de colaboración se crean por la interacción entre participantes en contraposición ``de simplemente depender de la presencia en escenarios naturales''. En los sistemas que se crean se comparte la información sobre la actividad que realizan, mientras aumenta el flujo de beneficios derivados de su intercambio, lo cual genera una regulación y monitoreo en que se involucran los participantes. Para lograr esto último es necesario que el bien común se cree a partir de las características culturales de los usuarios.

 En cuanto al vínculo de los bienes comunes con internet, esta última fue producto de la colaboración y construcción conjunta de expertos que supieron crear un arreglo cultural exitoso ya en 1945, con un grupo de entusiastas que según Eckert y Mauchly (responsables del invento de la computadora \textsc{eniac}), programaban por diversión. La costumbre cultural de los programadores entusiastas que creaban, manipulaban y compartían software por pura diversión toma forma en los setenta, remando a contracorriente por la decisión de reservar derechos en la producción intelectual en la naciente industria del cómputo durante los ochenta.

 Por esa razón, Richard Stallman convocó en 1982 a programar un clon de Unix,\footnote{Acrónimo de Uniplexed Information and Computing System, al que se le cambió la última letra. Sistema operativo multiusuario orientado al trabajo en red cuyo origen se remonta a finales de 1960.} llamado \textsc{gnu},\footnote{Acrónimo recursivo que significa \textsc{gnu}, \textit{no} es Unix.} y en 1985 se fundó la Free Software Foundation. La producción de \textsc{gnu} convocó a una nueva generación de \textit{hackers} a un proyecto común que tendría el final de su primer capítulo en 1993, cuando Linus Torvalds terminó su primera versión operativa y estable del núcleo que dotó a \textsc{gnu} de una funcionalidad con madurez sorpresiva hasta nuestros días. Así, mientras se consolidaba un sistema operativo, en 1994, formalmente se cerraba el grupo de desarrollo del Unix de Berkeley, último de los grupos de los primeros \textit{hackers}.

 Por aquella época (1989), Tim Berners-Lee y Robert Cailliau, otro par de \textit{hackers} que trabajaban en la Organización Europea para la Investigación Nuclear –conocida por las siglas \textsc{cern}–, inventaron la \textit{Word Wide Web}, con la finalidad de que la información académica estuviera disponible para cualquier persona y se fomentara el intercambio internacional de información en equipos dispersos \Parencite{858}. Así, se impulsó lo que hasta hoy es un estándar libre y abierto que se ha convertido en el motor principal de los cambios sociales contemporáneos.

 Sin embargo, no todos los movimientos vinculados con internet que promueven el intercambio de información están motivados por el bien común; de manera paradójica algunas comunidades han desarrollado mecanismos descentralizados en que se comparte buscando el beneficio personal pero simultáneamente sirven a la comunidad. Un ejemplo que ilustra a éste \textit{altruismo egoísta} es la tecnología par a par (\textsc{p2p}), un sistema descentralizado de compartición de archivos en el que cada usuario es un cliente que descarga información y un servidor que pone a disposición esa misma información para que otro la descargue. Se trata de un esquema de ``pérdida cero donde todos ganan, nadie pierde'' y, entre más beneficio personal hay, más archivos se comparten en la red; en palabras de Rheingold \parencite*{845}, se trata de ovejas electrónicas que cagan pasto.

 En este contexto de construcción colaborativa de conocimiento y compartición de información, se enmarca el software libre y sus principios éticos, que permiten tener un objeto definido con un eje moral que la da cohesión a la presencia social de una infinidad de comunidades de práctica sobre software libre en internet. Como ya lo mencionábamos, la presencia social alrededor del ejercicio de la programación libre genera un sentido de filiación y pertenencia de grupo, al que los participantes se suman voluntariamente y al que –mediante la acción mediada por la tecnología– descubren como un medio de expresión que les da ``voz'', les asigna un papel y en muchos casos los lleva a convertirse en guía actitudinal proactiva alrededor del concepto del software libre.

 Con el tiempo, nuevas áreas disciplinares adoptaron y adaptaron la propuesta de software libre, así que los principios básicos se amplían para garantizar bienes ``culturales libres'', como en el arte, la producción de textos, videos o la educación. Con el cambio de objeto emergen en internet proyectos sociales y educativos que buscan la libertad del conocimiento y se autodenominan de ``cultura libre'' orientados a producir ``objetos culturales libres''.

 Lawrence Lessig \parencite*[8]{840} aporta claridad a ese concepto, al explicar que la cultura libre se trata de:

\begin{quotation}
  [\ldots] una manera en la que se construye nuestra cultura[\ldots] Una cultura libre apoya y protege a creadores e innovadores. Esto lo hace directamente concediendo derechos de propiedad intelectual. Pero lo hace también indirectamente limitando el alcance de estos derechos, para garantizar que los creadores e innovadores que vengan más tarde sean tan libres como sea posible del control del pasado. Una cultura libre no es una cultura sin propiedad, del mismo modo que el libre mercado no es un mercado en el que todo es libre y gratuito. Lo opuesto a una cultura libre es una \textit{cultura del permiso} —en la cual los creadores logran generar sus obras solamente con el permiso de los poderosos, o de los creadores del pasado.
\end{quotation}



 Uno de los hitos de estas nuevas comunidades de producción de conocimiento libre es Wikipedia.\footnote{Referido en la sección \ref{sl_cds:soft_al_conoc}, ``Del software al conocimiento'', de la presente obra.} El otro gran hito es el movimiento de los Creative Commons o ``bienes comunes creativos'', según la traducción oficial de este mismo grupo.

 Creative Commons es una organización sin fines de lucro que busca el intercambio libre y legal de bienes comunes que garanticen el uso, reutilización y remezcla, bajo el lema ``compartir, rehacer y reusar, legalmente'', ofreciendo un servicio gratuito y fácil de utilizar con herramientas legales estandarizadas y localizadas en el ámbito geográfico para facilitar la cesión de permisos sobre derechos de autor o de explotación a terceros en trabajo creativos.

\subsection{De la cultura libre a la educación libre}

\noindent  Para el movimiento del software libre, el inicio del nuevo siglo significó una serie de aperturas fundamentales que provocarían una reestructura en el contexto del software libre y crearían las bases para el naciente concepto de cultura libre y sus implicaciones en la educación. Así:

\begin{description}
\item[2000] Se fundó la comunidad de ``Software de Libre Redistribución y Educación en Colombia'' \Parencite{852}, con el objetivo de informar, apoyar y organizar eventos y actividades sin ánimo de lucro relacionadas con software de libre redistribución y educación en Colombia.

\item[2001]

\begin{itemize}
\item  Inicia la operación del proyecto \textit{Wikipedia} \Parencite{853}.\footnote{Este punto se aborda con amplitud en el apartado ``Software libre y construcción democrática de la sociedad''.}

\item  Se fundó el proyecto Creative Commons \Parencite{854}.

\item  Se dió inició al proyecto \href{http://www.seul.org/}{Simple End-User Linux (\textsc{seul})} \Parencite{855}, que buscaba tener software libre accesible y robusto, lo suficientemente desarrollado para el usuario final. Esta asociación originó la coalición de instituciones educativas \Parencite{856}, que desarrollan recursos abiertos para educación.

\item  En el ámbito latinoamericano el Centro de Investigación Aplicada de \textsc{gnu}/Linux y Software Libre, en Argentina, convocó al primer encuentro virtual llamado \href{http://www.cignux.org.ar/encuentro.htm}{\textit \textsc{gnu}/Linux y Software Libre en Educación}, del que no se editaron nuevos capítulos.
\end{itemize}

\item[2002] Como resultado del primer encuentro virtual, se fundó la comunidad Gleducar \parencite*{857} como un ``proyecto educativo, colaborativo y cooperativo que persigue la adecuación de las aulas argentinas a las nuevas tecnologías de la comunicación y la información''.

\item[2004] Se fundó \href{http://educalibre.cl/}{EducaLibre,} comunidad educativa chilena, a partir de una reunión de educadores en el marco del V Encuentro Linux Nacional.
\end{description}

En este contexto de cambios del software libre a la cultura libre se creó un espacio de habla en español para el intercambio de ``experiencias, propuestas y opiniones entre la comunidad educativa interesada en el software libre con la finalidad de producir colaborativamente un cuerpo de conocimiento que permitiera a todos reflexionar'' sobre sus acciones, experiencias y propuestas, así que tomó forma el \href{http://edusol.info}{Encuentro en Línea de Educación y Software Libre \textit{Edusol}} (hoy de Educación, Cultura y Software Libres) (Miranda y Wolf, \cites*{841}{843}).

 Edusol tiene sus antecedentes en los trabajos del proyecto Investigación Psicoeducativa de la Universidad Nacional Autónoma de México, que desde 1997 usaba herramientas libres aplicadas a la educación y en el que no habían espacios formales para ventilar las preguntas y las experiencias desde la educación y para la educación.

 Retomo una \href{http://alejandromiranda.org/node/25}{entrada de bitácora que publiqué el 16 de marzo de 2007} en la que contextualizo los usos del software libre aplicados en la educación, particularmente el primer intento por crear una comunidad virtual de aprendizaje en un portal educativo denominado \textit{Biné: la comunidad académica en línea}.

\begin{quotation}
  \textit{Biné} salió públicamente en septiembre de 2001 (aunque los trabajos para prepararlo se remontan a 1999) en \url{http://bine.iztacala.unam.mx/ptl/} (hoy inexistente), como una apuesta educativa por reproducir los esquemas de colaboración y construcción de conocimiento que observaba en las comunidades de software libre.

  El primer nombre que tomó fue el de ``Programa de tutelaje en línea'' (\textsc{ptl}) y era un recurso para que los alumnos que tomaban el Laboratorio en Línea de Enseñanza de Cómputo se integraran en una comunidad en línea que autorregulara sus contenidos. El \textsc{ptl} terminó involuntariamente luego de varios cambios administrativos y recurrentes problemas para mantener el sitio en línea dentro de la Facultad de Estudios Superiores Iztacala de la Universidad Nacional Autónoma de México.

  Para ese entonces, ya se tenía clara la línea que habría de seguir \textit{Biné} (que en rarámuri quiere decir \textit{aprender}): un sitio en que se privilegiara el análisis y discusión de temas diversos que tuvieran como eje la educación, la ciencia y la cultura. Así, \textit{Biné} fue redirigido de octubre de 2004 y hasta marzo de 2005 a \url{http://gamd.ath.cx/bine} (ahora \url{http:// alejandromiranda.org}), para luego mudarse a su casa actual: \url{http://bine.org.mx}. Hoy \textit{Biné} continúa con su servicio de bitácoras educativas y con diversas líneas especializadas.

  \begin{itemize}
  \item  El encuentro en línea Educación y Software Libre \textit{Edusol} 2005, 2006 y 2007.

  \item  El \textit{Planeta Edusol}, colección de bitácoras personales de los interesados en la educación y software libre.

  \item  Xoti: herramientas libres para la vida. Busca generar un espacio aplicado de alfabetización digital que ayude a reducir las diferencias de acceso de la información y uso de la tecnología de los que menos tienen.

  \item  Pädi: la colaboración en línea. Se imparten cursos diversos sobre la plataforma Moodle.
  \end{itemize}
\end{quotation}



 Casi tres años después de esta entrada, el proyecto Edusol ha crecido lo suficiente para mantener su domino propio y su comunidad, y para albergar subproyectos orientados a la promoción de la cultura libre en el ámbito escolar.

\subsection{Bienes culturales libres: dibujando\\ el futuro}

\noindent  La cronología anterior sólo es un botón de muestra acerca de cómo el tema de la educación y el software libre en un decenio ha dado un giro importante pasando de las propuestas \textit{softwarelibrecentristas} a resaltar la relevancia para la educación de las implicaciones éticas y cívicas de las propuestas culturales libres.

 Esto es un indicador de que el tema va madurando e involucrando nuevos discursos que ya no se centran en el código, como resultado del involucramiento y toma de poder de personajes que provienen de disciplinas no tecnólogas y que se encuentran en la búsqueda activa de nuevas fórmulas de inclusión, en las que el software libre sea un prerrequisito para construir una mejor sociedad a partir de acuerdos éticos mínimos que garanticen una mejor convi\-vencia; desde un movimiento contracultural, en un mundo en el que aparentemente ha sido cooptado por el dominio de empresas que monopolizan el acceso y divulgación del conocimiento.
